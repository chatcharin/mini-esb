/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailverifier;

import static ccsthread.getUnicode.getHEX;
import static ccsthread.getUnicode.getValue;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;
import javax.swing.JOptionPane;
/**
 *
 * @author Aek
 */
public class MSSQL_Connect {

    int tableNum = 12;
    //int[] lastCount = new int[tableNum];
    Timestamp[] lastTime = new Timestamp[tableNum];
    
    //table array
    ArrayList<String[]>[] tables = new ArrayList[tableNum];
    
    //MSSQL
    Connection con;
    //PostgreSQL
    Connection pcon;
    
    Connection gcon;
    //Message
    DataManagement dm = null;
    
    public String msg = "DataSynchro started";
    public boolean error = false;
    String wms_host,wms_dbname,wms_username,wms_passwd,wms_port,fml_host,fml_dbname,fml_username,fml_passwd,fml_port = "";
    void getProperty(){
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("setting.properties"));
            wms_host=prop.getProperty("wms_host");
            wms_dbname=prop.getProperty("wms_dbname");
            wms_username=prop.getProperty("wms_username");
            wms_passwd=prop.getProperty("wms_passwd");
            wms_port=prop.getProperty("wms_port");
            fml_host=prop.getProperty("fml_host");
            fml_dbname=prop.getProperty("fml_dbname");
            fml_username=prop.getProperty("fml_username");
            fml_passwd=prop.getProperty("fml_passwd");
            fml_port=prop.getProperty("fml_port");
        } catch (IOException ex) {
            Logger.getLogger(setting.class.getName()).log(Level.SEVERE, null, ex);
              JOptionPane.showMessageDialog(null,ex.getMessage());
        }
    }
    
    
    public MSSQL_Connect(){
        getProperty();  
        newTables();       
        //Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
        //con = DriverManager.getConnection("jdbc:sqlserver://203.154.232.7:1433;databaseName=Formula1","sa","P@ssword");
        getMSSQL();
        try{
        getMySQL();
        }catch(Exception e){}
        dm = new DataManagement();
       // getPostgreSQL();
        //getColumn();
    }
    String time[] = null;
    public void newTables(){
        //new tables
        p("Start Create Tmp Table ...... ");
        tables[0] = new ArrayList<String[]>();
        tables[1] = new ArrayList<String[]>();
        tables[2] = new ArrayList<String[]>(); 
        tables[3] = new ArrayList<String[]>();
        tables[4] = new ArrayList<String[]>();
        tables[5] = new ArrayList<String[]>();
        tables[6] = new ArrayList<String[]>();
        tables[7] = new ArrayList<String[]>();
        tables[8] = new ArrayList<String[]>();
        tables[9] = new ArrayList<String[]>();
        tables[10] = new ArrayList<String[]>(); // new id
        tables[11] = new ArrayList<String[]>(); // new id 
        time = new String[12];
        p("Finish Create Table ......");
    }
    public void p(String txt){
        System.out.println("----"+txt);
    }
    public void getMSSQL(){
        //MSSQL
        p("Create Connection ... ");
        msg = "Connecting to \nMicrosoft SQL server"; 
        try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
//             con = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=formula1","sa","1234");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
               pcon = DriverManager.getConnection("jdbc:sqlserver://"+wms_host+":"+wms_port+";databaseName="+wms_dbname+"",wms_username,wms_passwd);
               gcon =  DriverManager.getConnection("jdbc:sqlserver://"+fml_host+":"+fml_port+";databaseName="+fml_dbname+"",fml_username,fml_passwd);
               con = DriverManager.getConnection("jdbc:sqlserver://"+fml_host+":"+fml_port+";databaseName="+fml_dbname+"",fml_username,fml_passwd);
         } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
            //error = true;
         }
        p("Finish Connection...");
        
    }
    public void reconnect(){
         try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
             if(pcon.isClosed()){
                 pcon = DriverManager.getConnection("jdbc:sqlserver://"+wms_host+":"+wms_port+";databaseName="+wms_dbname+"",wms_username,wms_passwd);
                 msg = "Reconnect WMS!";
             }
             if(gcon.isClosed()){
                gcon =  DriverManager.getConnection("jdbc:sqlserver://"+fml_host+":"+fml_port+";databaseName="+fml_dbname+"",fml_username,fml_passwd);
                 msg = "Reconnect Formula !"; 
             }
              if(con.isClosed()){
                 con =  DriverManager.getConnection("jdbc:sqlserver://"+fml_host+":"+fml_port+";databaseName="+fml_dbname+"",fml_username,fml_passwd);
                 msg = "Reconnect Formula !"; 
             }
         } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed reconnect!";
            //error = true;
         }
    }
    Connection mycon;
    public void getMySQL(){
        //MSSQL
        msg = "Connecting to \nMySQL SQL "; 
        try {
             Class.forName("com.mysql.jdbc.Driver").newInstance();
//             con = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=formula1","sa","1234");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
               mycon = DriverManager.getConnection("jdbc:mysql://localhost:3306/map_data?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull","root","");
             
         } catch (Exception e) {
            System.out.println(e);
          //  msg = "Connection failed !";
           // error = true;
         }
    }
    public void getPostgreSQL(){
        //PostgreSQL
         msg = "Connecting to \nPostgreSQL server"; 
         try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
             //pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
             pcon = DriverManager.getConnection("jdbc:sqlserver://"+wms_host+":"+wms_port+";databaseName="+wms_dbname+"",wms_username,wms_passwd);
         } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
            //error = true;
         }
    }
    
    public void checkUpdate(){
        String step = "0";
        try{  
            reconnect();
            step = "1";
            for(int m=0;m<tableNum;m++){
                checkModified(m);
            }
             step = "2";
            newTables();
             step = "3";
            System.gc();
             step = "4";
            initTime();
             step = "5";
        }catch(Exception e){ 
            System.out.println(e.getMessage()+step); 
            if(e.getMessage()==null)
                reset = true;
            msg = "Fatal error !"+step;
            //error = true;
            
        }
    }
   boolean reset = false ;
    public void initTime(){
        p("get time last update....");
        msg = "Getting last updated\ntime information"; 
        for(int n=0;n<tableNum;n++){
           lastTime[n] =getLastTimeByField(n);
                   //Timestamp.valueOf("2013-06-04 16:29:40.55");
                   //Timestamp.valueOf("2013-04-20 16:29:40.55");
                   //getLastTime(n);
           if(n== 6) lastTime[n] = Timestamp.valueOf("2020-11-01 16:29:40.55");
           if(n > 9 )
                lastTime[n] =getLastTimeByField(n);
                        //Timestamp.valueOf("2013-06-04 16:29:40.55");// getLastTime(n);
                        //Timestamp.valueOf("2012-11-01 16:29:40.55");
            System.out.println(lastTime[n].toString());
        }
    }
    public void updateLast(String table,String time){
        try {
            Statement stmt = mycon.createStatement();
            System.out.println("INSERT INTO `map_data`.`last_update`(`id`, `table_update`, `time`) VALUES (NULL,'"+table+"','"+time+"');");
            stmt.execute("INSERT INTO `map_data`.`last_update`(`id`, `table_update`, `time`) VALUES (NULL,'"+table+"','"+time+"');");
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(MSSQL_Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Timestamp getLastTimeByField(int num){
        Timestamp time = null; 
        String[] tableNames = new String[]{"approd_detail","order_lines","purchase_orders","sales_details","sales_orders","delivery_detail","customer_master","suppliers","product_master","product_barcode","GLREF","REFPROD"};
        String[] field     = {"","udf5","remarks5","udf5","vessel","delivery","seq_group","supplier_category","healthcare_data","","FMMEMDATAA","FCMNMGL"};
        try { 
                if(!field[num].equalsIgnoreCase("")){ p("not emty");
                if(num<10){                 // Statement stmt = mycon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                         Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                          if(num==0){
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                   String sq = "SELECT \"approved_date\" FROM \""+tableNames[num]+"\" where \""+field[num]+"\"='sy' ORDER BY \"approved_date\" DESC ;";p(sq);
                                    ResultSet prs = stmt.executeQuery(sq);
                                    if(prs.next()){time = prs.getTimestamp(1);}         
                                    if(time==null){time = Timestamp.valueOf("2013-05-01 00:00:00.0");}
                                    prs.close();stmt.close();
                          }else{
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                   String sq = "SELECT \"last_update\" FROM \""+tableNames[num]+"\" where \""+field[num]+"\"='sy' ORDER BY \"last_update\" DESC ;";p(sq);
                                   ResultSet prs = stmt.executeQuery(sq);
                                   if(prs.next()){ time = prs.getTimestamp(1); }
                                   if(time==null){ time = Timestamp.valueOf("2013-05-01 00:00:00.0");}
                                 //  prs.close(); stmt.close();
                         }
                  }else{
                         Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                         String sq = "SELECT \"FTLASTEDIT\" FROM \""+tableNames[num]+"\" where \""+field[num]+"\"='sy' ORDER BY \"FTLASTEDIT\" DESC ;";p(sq);
                         ResultSet prs = stmt.executeQuery(sq);
                         if(prs.next()){time = prs.getTimestamp(1);}
                         if(time==null){time = Timestamp.valueOf("2013-05-01 00:00:00.0");}
                       //  prs.close();stmt.close();
                  }
                }else{ p("isemty");
                      Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                          if(num==0){
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                    ResultSet prs = stmt.executeQuery("SELECT \"approved_date\" FROM \""+tableNames[num]+"\"  ORDER BY \"approved_date\" DESC ;");
                                    System.out.println("SELECT \"approved_date\" FROM \""+tableNames[num]+"\" ORDER BY \"approved_date\" DESC ;");
                                    if(prs.next()){time = prs.getTimestamp(1);}         
                                    if(time==null){time = Timestamp.valueOf("2013-05-01 00:00:00.0");}
                                 //   prs.close();stmt.close();
                          }else{
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                   ResultSet prs = stmt.executeQuery("SELECT \"last_update\" FROM \""+tableNames[num]+"\" ORDER BY \"last_update\" DESC ;");
                                   if(prs.next()){ time = prs.getTimestamp(1); }
                                   if(time==null){ time = Timestamp.valueOf("2013-05-01 00:00:00.0");}
                               //    prs.close(); stmt.close();
                         }
                }
            
        } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
          //  error = true;
        }
        return time;
    }
    
     public Timestamp getLastTime(int num){
        Timestamp time = null; 
        String[] tableNames = new String[]{"approd_detail","order_lines","purchase_orders","sales_details","sales_orders","delivery_detail","customer_master","suppliers","product_master","product_barcode","GLREF","REFPROD"};
        try { 
                if(num<10){
            
                         // Statement stmt = mycon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                         Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                          if(num==0){
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                    ResultSet prs = stmt.executeQuery("SELECT \"approved_date\" FROM \""+tableNames[num]+"\" ORDER BY \"approved_date\" DESC ;");
                                    System.out.println("SELECT \"approved_date\" FROM \""+tableNames[num]+"\" ORDER BY \"approved_date\" DESC ;");
                                    if(prs.next()){
                                      time = prs.getTimestamp(1);
                                    }
                                    
                                        if(time==null){
                                        time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                                        }
                                        //    prs.close();
                                        //    stmt.close();
                          }else{
                                  //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                                   ResultSet prs = stmt.executeQuery("SELECT \"last_update\" FROM \""+tableNames[num]+"\" ORDER BY \"last_update\" DESC ;");
                                    if(prs.next()){
                                    time = prs.getTimestamp(1);
                                    }
                                    
                                         if(time==null){
                                         time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                                         }
                                         //    prs.close();
                                         //    stmt.close();
                         }
            
            
                  }else{
                         Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                         ResultSet prs = stmt.executeQuery("SELECT \"FTLASTEDIT\" FROM \""+tableNames[num]+"\" ORDER BY \"FTLASTEDIT\" DESC ;");
                         if(prs.next()){
                         time = prs.getTimestamp(1);
                         }
                                if(time==null){
                                time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                                }
                                    prs.close();
                                    stmt.close();
                  }
            
        } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
          //  error = true;
        }
        return time;
    }
    
    public void checkModified(int num){
        msg = "Synchronizing . . .";
        //queries    
        //String last = lastTime[num].toString();
        String[] queries = new String[]{ 
        "SELECT FCREFNO,FDDATE FROM dbo.ORDERH WHERE FDDATE>CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY FDDATE ASC",    
        "SELECT dbo.ORDERH.FCCORP, dbo.ORDERH.FCREFNO, dbo.PROD.FCCODE, dbo.COOR.FCNAME AS Expr1, dbo.UM.FCNAME, dbo.ORDERI.FNUMQTY, dbo.ORDERH.FDDATE, dbo.ORDERH.FCSTAT, dbo.ORDERH.FNAMT, dbo.ORDERI.FNPRICE,dbo.ORDERH.FNDISCAMTK, dbo.ORDERH.FTLASTUPD FROM dbo.COOR LEFT OUTER JOIN dbo.ORDERH ON dbo.COOR.FCSKID = dbo.ORDERH.FCCOOR FULL OUTER JOIN UM INNER JOIN dbo.ORDERI ON dbo.UM.FCSKID = dbo.ORDERI.FCUM ON dbo.ORDERH.FCSKID = dbo.ORDERI.FCORDERH  left join PROD on ORDERI.FCPROD = PROD.FCSKID WHERE dbo.COOR.FCISSUPP = 'Y' AND dbo.ORDERH.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.ORDERH.FTLASTUPD ASC",
        "SELECT dbo.ORDERH.FCCORP, dbo.ORDERH.FCREFNO, dbo.ORDERI.FCREFTYPE, dbo.ORDERH.FCCODE, dbo.ORDERH.FCSTAT, dbo.COOR.FCCODE, dbo.COOR.FCSNAME, dbo.ORDERH.FNCREDTERM, dbo.SUBBR.FCADDR1, dbo.CURRENCY.FCNAME AS Expr1,dbo.ORDERH.FTLASTUPD FROM  ORDERH left JOIN ORDERI ON ORDERH.FCSKID = ORDERI.FCSKID left JOIN SUBBR ON ORDERH.FCSKID = SUBBR.FCSKID left JOIN CURRENCY ON ORDERH.FCSKID = CURRENCY.FCSKID left join COOR ON ORDERH.FCCOOR = COOR.FCSKID WHERE dbo.ORDERH.FTLASTUPD >convert(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.ORDERH.FTLASTUPD ASC",
        
        "SELECT dbo.ORDERH.FCCORP, dbo.COOR.FCCODE, dbo.ORDERH.FCREFNO, dbo.ORDERH.FCSKID, dbo.PROD.FCCODE, dbo.PROD.FCSNAME, dbo.UM.FCCODE, dbo.ORDERI.FNUMQTY, dbo.ORDERI.FNPRICE, dbo.PROD.FNSTDCOST,dbo.ORDERH.FTLASTUPD, dbo.ORDERH.FCCREATEAP, dbo.ORDERH.FDDATE , dbo.ORDERI.FNQTY , dbo.ORDERI.FNBACKQTY FROM dbo.ORDERH LEFT OUTER JOIN dbo.ORDERI ON dbo.ORDERH.FTLASTUPD = dbo.ORDERI.FTLASTUPD FULL OUTER JOIN dbo.PROD ON dbo.ORDERI.FCPROD = dbo.PROD.FCSKID INNER JOIN dbo.COOR ON dbo.ORDERH.FCCOOR = dbo.COOR.FCSKID INNER JOIN dbo.UM ON dbo.ORDERI.FCUM=dbo.UM.FCSKID WHERE dbo.ORDERH.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.ORDERH.FTLASTUPD ASC",
        "SELECT dbo.ORDERH.FCCORP, dbo.ORDERH.FCREFNO, dbo.COOR.FCCODE, dbo.COOR.FCSNAME, dbo.ORDERH.FNVATRATE, dbo.ORDERH.FNDISCAMT1, dbo.ORDERH.FMMEMDATA, dbo.ORDERH.FCDELICOOR, dbo.ORDERH.FDDATE, dbo.ORDERH.FTLASTUPD FROM dbo.ORDERH INNER JOIN dbo.COOR ON dbo.ORDERH.FCCOOR = dbo.COOR.FCSKID WHERE dbo.ORDERH.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.ORDERH.FTLASTUPD ASC",
        "SELECT dbo.ORDERI.FCSKID, dbo.ORDERI.FCCORP, dbo.ORDERI.FCPROD, dbo.ORDERI.FCCOOR, dbo.ORDERI.FNUMQTY, dbo.ORDERI.FCUM, dbo.ORDERI.FDDATE, dbo.ORDERI.FTLASTUPD FROM dbo.ORDERI FULL OUTER JOIN dbo.ORDERH ON dbo.ORDERI.FCSKID = dbo.ORDERH.FCSKID WHERE dbo.ORDERI.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.ORDERH.FTLASTUPD ASC",
        
        "SELECT dbo.COOR.FCCORP, dbo.COOR.FCCODE, dbo.COOR.FCNAME, dbo.COOR.FMMEMDATA, dbo.COOR.FCTEL, dbo.CRGRP.FCNAME AS Expr1, dbo.COOR.FNCREDTERM , dbo.COOR.FTLASTUPD FROM dbo.COOR RIGHT OUTER JOIN dbo.CRGRP ON dbo.COOR.FCCRGRP = dbo.CRGRP.FCSKID WHERE dbo.COOR.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.COOR.FTLASTUPD ASC",
        "SELECT FCCORP,FCCODE,FCNAME2,FCADDR1,FTLASTUPD FROM dbo.SUBBR WHERE FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY FTLASTUPD ASC",
        "SELECT FCCORP,FCSKID,FCTYPE,FCNAME,FNPRICE,FTLASTUPD from dbo.PROD WHERE FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY FTLASTUPD ASC",
        "SELECT dbo.PROD.FCCORP, dbo.PROD.FCSKID, dbo.PROD.FNUMQTY1, dbo.PROD.FCUM, dbo.STOCK.FNSTQTYSTD, dbo.PROD.FTLASTUPD FROM dbo.PROD RIGHT OUTER JOIN dbo.STOCK ON dbo.PROD.FCSKID = dbo.STOCK.FCPROD WHERE dbo.PROD.FTLASTUPD >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.PROD.FTLASTUPD ASC",
        
        "SELECT dbo.invoice_master."
                + "invoice_no,  dbo.invoice_master.client,"
                + "  dbo.invoice_master.warehouse,"
                + "  dbo.invoice_master.invoice_date,"
                + "  dbo.invoice_master.created_date,"
                + "  dbo.invoice_master.delivery_date,"
                + "  dbo.invoice_master.cus_po_no,"
                + "  dbo.invoice_master.sales_order,"
                + " SUM( dbo.invoice_detail.unit_price) AS Expr1,"
                + "  dbo.invoice_master.vat,"
                + "  dbo.invoice_master.customer,  dbo.invoice_master.maker,  dbo.invoice_master.payment_term,  dbo.invoice_master.delivery,  dbo.invoice_master.last_update,  dbo.invoice_master.delivery_date FROM dbo.invoice_master CROSS JOIN dbo.invoice_detail GROUP BY  dbo.invoice_master.invoice_no,  dbo.invoice_master.client,  dbo.invoice_master.warehouse,  dbo.invoice_master.invoice_date,  dbo.invoice_master.created_date,  dbo.invoice_master.due_date,  dbo.invoice_master.cus_po_no, dbo.invoice_master.sales_order,  dbo.invoice_master.vat,  dbo.invoice_master.customer,  dbo.invoice_master.maker,  dbo.invoice_master.payment_term,  dbo.invoice_master.delivery,  dbo.invoice_master.last_update, dbo.invoice_master.delivery_date HAVING ( dbo.invoice_master.last_update > CONVERT(DateTime,'"+lastTime[num].toString()+"')) ORDER BY dbo.invoice_master.last_update ASC",
        "SELECT dbo.invoice_detail.client, dbo.invoice_detail.warehouse, dbo.invoice_detail.work_order, dbo.invoice_detail.invoice_no, dbo.invoice_master.invoice_date, dbo.invoice_detail.customer, dbo.invoice_detail.item_no, dbo.invoice_detail.uom, dbo.invoice_detail.qty, dbo.invoice_detail.unit_price, dbo.invoice_master.vat, dbo.invoice_detail.so_line_no, dbo.invoice_master.remark, dbo.invoice_detail.discount, dbo.invoice_detail.last_update, dbo.invoice_master.delivery_date, dbo.invoice_detail.maker FROM dbo.invoice_detail FULL OUTER JOIN dbo.invoice_master ON dbo.invoice_detail.row_id = dbo.invoice_master.row_id WHERE dbo.invoice_detail.last_update >CONVERT(DateTime,'"+lastTime[num].toString()+"') ORDER BY dbo.invoice_detail.last_update ASC"        
                
            
        };
      p("Start Check Modify...");
          try {
              if(num<10){
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(queries[num]);
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int column = rsMetaData.getColumnCount();
            while(rs.next()){
                String[] values = new String[column];
                
              for(int a=0;a<column;a++){
                
                  values[a] = rs.getString(a+1);    
                 
              }
                tables[num].add(values);
          }
                 //rs.close(); stmt.close();
              }else{
                  Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(queries[num]);
            ResultSetMetaData rsMetaData = rs.getMetaData();
            int column = rsMetaData.getColumnCount();
            while(rs.next()){
                String[] values = new String[column];
                
              for(int a=0;a<column;a++){
                
                  values[a] = rs.getString(a+1);    
                 
              }
                tables[num].add(values);
          }
                 //rs.close(); stmt.close();
              }
            
            
            //send data
            if(tables[num].size()>0){
                //new lastTime              
               lastTime[num] = getLastTime(num);
                postgresQuery(num);
            }

        } catch (Exception e) {
            System.out.println("Check Modifi "+e);
            msg = "Connection failed !";
          //  error = true;
        }
    }

    
    String system[] ={"WMS","WMS","WMS","WMS","WMS","WMS","WMS","WMS","WMS","WMS","Formula","Formula"};
    String table[]  ={"approd_detail","order_lines","purchase_orders","sales_details","sales_orders","delivery_detail","customer_master","suppliers","product_master","product_barcode","GLREF","REFPROD"};
  
    public String ReSymbol(String txt){
       String tmp_sql = txt.replaceAll("\"","\\\\\"");
       String sql_er = tmp_sql.replaceAll("'","\\\\'");
        return sql_er;
    }
    
    
    public String getSQL(String sys,String tbl,String sql,String er){
        //WHERE NOT EXISTS ( SELECT * FROM users nx WHERE nx.ip = xx -- KEYFIELDS);
        return "INSERT INTO `map_data`.`data_log` (`id`,`system`, `table`, `sql_txt`, `error`, `time`) VALUES (NULL, '"+sys+"', '"+tbl+"', '"+ReSymbol(sql)+"', '"+ReSymbol(er)+"', CURRENT_TIMESTAMP) ;";
    }
    public static boolean isNumber(String text) {
  try {
    new Double(text);
    return true;
  } catch (NumberFormatException e) {
    return false;
  }
}
    
    public void postgresQuery(int num){
        msg = "Updating . . .";
        //tables[num].size()
        for(int i=0;i<tables[num].size();i++){       
            //queries
             String[] queries = new String[]{
          "INSERT INTO \"approd_detail\"(\"po_no\",\"level\",\"status\",\"detail\",\"approved_date\") VALUES ('",
          "INSERT INTO \"order_lines\"(\"client\",\"order_no\",\"item_no\",\"supplier\",\"uom\",\"qty\",\"uom_po\",\"grade\",\"last_receipt_no\",\"status\",\"amount\",\"unit_price\",\"discount\",\"last_update\",\"udf5\") VALUES ('", 
          "INSERT INTO \"purchase_orders\"(\"client\",\"order_no\",\"order_type\",\"reference_no\",\"status\",\"supplier\",\"supplier_name\",\"payment_term\",\"street\",\"bldg\",\"town\",\"state\",\"country\",\"phone\",\"fax\",\"transport_mode\",\"currency\",\"transport_charge\",\"last_update\",\"remarks5\") VALUES ('",

          "INSERT INTO \"sales_details\"(\"client\",\"customer\",\"reference_no\",\"order_no\",\"org_item\","
                                       + "\"item_no\",\"item_description\",\"uom\",\"qty\",\"qty_picked\","
                                       + "\"qty_backorder\",\"unit_price\",\"unit_cost\",\"status\",\"displayed_status\","
                                       + "\"grade\",\"pallet\",\"location_picked\",\"remarks\",\"last_update\","
                                       + "\"maker\",\"created_date\",\"created_by\",\"margin\",\"gp\","
                                       + "\"udf5\") VALUES ('",
          "INSERT INTO \"sales_orders\"(\"client\",\"order_no\",\"customer\",\"customer_name\",\"wave_pick_no\","
                                     + "\"vat_rate\",\"discount_rate\",\"hawb_no\",\"order_type\",\"consignment_no\","
                                     + "\"bill_to\",\"bill_street\",\"bill_bldg\",\"bill_street3\",\"bill_town\","
                                     + "\"dlvr_code\",\"dlvr_to\",\"dlvr_street\",\"dlvr_bldg\",\"dlvr_street3\","
                                     + "\"dlvr_town\",\"status\",\"despatch\",\"delivery_date\",\"pick_date\",\"backorder\",\"last_update\",\"vessel\") VALUES ('", 
          "INSERT INTO \"delivery_detail\"(\"con_note\",\"sales_order\",\"container\",\"client\",\"item_no\",\"customer\",\"qty\",\"uom\",\"grade\",\"receipt\",\"expiry\",\"warehouse\",\"maker\",\"last_update\",\"delivery\") VALUES ('",
         
          "INSERT INTO \"customer_master\"(\"client\",\"customer\",\"customer_name\",\"contact_person\",\"street\",\"town\",\"state\",\"country\",\"post_code\",\"phone\",\"fax\",\"email\",\"customer_group\",\"customer_category\",\"sales_name\",\"payment_term\",\"last_update\",\"seq_group\") VALUES ('", 
          "INSERT INTO \"suppliers\"(\"client\",\"supplier\",\"supplier_name\",\"contact_person\",\"street\",\"building\",\"street3\",\"town\",\"state\",\"country\",\"post_code\",\"phone\",\"fax\",\"email\",\"status\",\"supplier_category\",\"last_update\",\"supplier_category\") VALUES ('",
          "INSERT INTO \"product_master\"(\"client\",\"item_no\",\"product_class\",\"description\",\"pallet_config\",\"expiry_prealert\",\"shelf_life\",\"unit_price\",\"default_grade\",\"pick_seq\",\"last_update\",\"healthcare_data\") VALUES ('",
          "INSERT INTO \"product_barcode\"(\"client\",\"barcode_no\",\"item_no\",\"pack_uom\",\"base_uom\",\"convert_qty\",\"last_update\",\"maker\",\"remark\") VALUES ('",

          "INSERT INTO \"GLREF\"(\"FCSKID\",\"FCUDATE\",\"FCDATASER\",\"FCLUPDAPP\",\"FCRFTYPE\",\"FCREFTYPE\",\"FCCORP\",\"FCBRANCH\",\"FCDEPT\",\"FCSECT\","
                     + "\"FCJOB\",\"FCSTAT\",\"FCSTEP\",\"FCACSTEP\",\"FCGLHEAD\",\"FCGLHEADCA\",\"FDDATE\",\"FDRECEDATE\",\"FDDUEDATE\",\"FDLAYDATE\","
                     + "\"FDDEBTDATE\",\"FCDEBTCODE\",\"FCBOOK\",\"FCCODE\",\"FCREFNO\",\"FNDISCAMTI\",\"FNDISCAMT1\",\"FNDISCAMT2\",\"FNDISCAMTA\",\"FNDISCPCN1\","
                     + "\"FNDISCPCN2\",\"FNDISCPCN3\",\"FNAMT\",\"FNPAYAMT\",\"FNSPAYAMT\",\"FNBEFOAMT\",\"FCVATISOUT\",\"FCVATTYPE\","
                     + "\"FNVATRATE\",\"FNVATAMT\",\"FNAMT2\",\"FNAMTNOVAT\",\"FCCOOR\",\"FCEMPL\",\"FNCREDTERM\",\"FCISCASH\",\"FCHASRET\",\"FCVATDUE\","
                     + "\"FNDEBTZERO\",\"FCLAYH\",\"FCLAYSEQ\",\"FCFRWHOUSE\",\"FCTOWHOUSE\",\"FCCREATEBY\",\"FCCORRECTB\",\"FCCANCELBY\",\"FMMEMDATA\",\"FCSEQ\","
                     + "\"FCPROMOTE\",\"FCVATCOOR\",\"FCRECVMAN\",\"FCSTEPX1\",\"FCSTEPX2\",\"FCCREATETY\",\"FCEAFTERR\",\"FCATSTEP\",\"FNVATPORT\",\"FNVATPORTA\","
                     + "\"FCMACHINE\",\"FCPERIODS\",\"FCSELTAG\",\"FTDATETIME\",\"FIMILLISEC\",\"FCUTIME\",\"FCCASHIER\",\"FCTXQCBRAN\",\"FCTXQCWHOU\",\"FCTXQCBOOK\","
                     + "\"FCTXQCGLRE\",\"FCTXBRANCH\",\"FCTXWHOUSE\",\"FCTXBOOK\",\"FCAPPROVEB\",\"FDFULLFILL\",\"FCPROJ\",\"FCDISCSTR\",\"FCCURRENCY\",\"FNXRATE\",\"FNAMTKE\",\"FNVATAMTKE\"," 
                     + "\"FNPAYAMTKE\",\"FNDISCAMTK\",\"FNBPAYINV\",\"FCVATSEQ\",\"FCTIMESTAM\",\"FCTRANWHY\",\"FCDELICOOR\",\"FCSUBBR\",\"FCMORDERH\",\"FCPLANT\",\"FTLASTUPD\",\"FCXFERSTEP\",\"FTXFER\",\"FNSTOCKUPD\",\"FCDOCOWNER\",\"FCREFDO\",\"FCINVIA\"," 
                     + "\"FCOUTVIA\",\"FCVLDCTRL\",\"FCPCONTRAC\",\"FCQCMO\",\"FCQCMOBK\",\"FCHASCHAIN\",\"FMMEMDATA2\",\"FMMEMDATA3\",\"FMMEMDATA4\",\"FCSHOWPLED\",\"FNPPAYAMT\",\"FNPPAYAMTK\",\"FDVATDATE\",\"FNLOCKED\",\"FNRETENAMT\",\"FMMEMDATA5\",\"FCTXID\"," 
                     + "\"FCSUBTXID\",\"FCTRADETRM\",\"FCDELIEMPL\",\"FCLAYMETHD\",\"FCCONSIGN\",\"FCSTEPD\",\"FTRECEIVE\",\"FCLID\",\"FTLASTEDIT\",\"FCPAYTERM\",\"FCU1STATUS\",\"FCU2STATUS\",\"FCDTYPE1\",\"FCDTYPE2\",\"FNDEPAMT\",\"FNDEPAMTKE\",\"FNBEFDEP\"," 
                     + "\"FNBEFDEPKE\",\"FNU1CNT\",\"FNU2CNT\",\"FMERRMSG\",\"FCCREATEAP\",\"FNEXPAMT\",\"FCPERSON\",\"FCLINKAPP1\",\"FCLINKSTP1\",\"FCLINKAPP2\",\"FCLINKSTP2\",\"FCCOUNTER\",\"FCPDBRAND\",\"FNCRXRATE\",\"FDAPPROVE\",\"FMMEMDATA6\",\"FMMEMDATA7\",\"FMMEMDATA8\","
                     + "\"FMMEMDATA9\",\"FMMEMDATAA\",\"FNAFTDEP\",\"FNAFTDEPKE\",\"FCCHKDT\",\"FCRECALBY\",\"FTLASRECAL\",\"FCMNMGLH\",\"FCMNMGLHCA\") VALUES (",
          
          "INSERT INTO \"REFPROD\"(\"FCSKID\",\"FCCORP\",\"FCDATASER\",\"FCUDATE\",\"FCUTIME\",\"FCLUPDAPP\",\"FCSTAT\",\"FCBRANCH\",\"FCDEPT\",\"FCSECT\",\"FCJOB\",\"FCGLHEAD\",\"FCGLREF\",\"FDDATE\",\"FCCOOR\",\"FCREFPDTYP\"," 
                     + "\"FCIOTYPE\",\"FCRFTYPE\",\"FCREFTYPE\",\"FCPRODTYPE\",\"FCSHOWCOMP\",\"FCROOTSEQ\",\"FCPFORMULA\",\"FCFORMULAS\",\"FCPROD\",\"FCUM\",\"FCUMSTD\",\"FNUMQTY\",\"FNQTY\",\"FCSTUM\",\"FCSTUMSTD\" ,"
                     + "\"FNSTUMQTY\",\"FNSTQTY\",\"FNPRICE\",\"FNCOST\",\"FNDISCAMT\",\"FNDISCPCN\",\"FCDISCSTR\",\"FNQTYATDAT\",\"FCVATISOUT\",\"FCVATTYPE\",\"FNVATRATE\",\"FNVATAMT\",\"FCSEQ\",\"FCLOT\" ,"
                     + "\"FCWHOUSE\",\"FMREMARK\",\"FNCOMMISSI\",\"FCPROMOTE\",\"FCACCHART\",\"FCCREATETY\",\"FCEAFTERR\",\"FCSELTAG\",\"FTDATETIME\",\"FIMILLISEC\",\"FNVATPORT\",\"FNVATPORTA\",\"FCPROJ\" ,"
                     + "\"FNCOSTAMT\",\"FNPRICEKE\",\"FNDISCAMTK\",\"FNXRATE\",\"FNCOSTADJ\",\"FMREMARK2\",\"FMREMARK3\",\"FMREMARK4\",\"FMREMARK5\",\"FMREMARK6\",\"FMREMARK7\",\"FMREMARK8\",\"FMREMARK9\" ,"
                     + "\"FMREMARK10\",\"FMMEMDATA\",\"FCTRANWHY\",\"FCTIMESTAM\",\"FCQCPASS\",\"FCGVPOLICY\",\"FCMFGRUNB\",\"FCMFGRUNE\",\"FCSUBBR\",\"FCPLANT\",\"FTLASTUPD\",\"FCLAYSTEP\",\"FCPROCHAIN\" ,"
                     + "\"FCXFERSTEP\",\"FTXFER\",\"FCREFDO\",\"FDEXPIRE\",\"FCSTORE\",\"FCGAG\",\"FMTEXTKE\",\"FDMFGDATE\",\"FCTXNO\",\"FCRECOSTTY\",\"FNDEFAPRIC\",\"FCGL\",\"FNREFQTY\",\"FCTXID\",\"FCSUBTXID\" ,"
                     + "\"FDDELIVERY\",\"FCMORDERH\",\"FCSTEPD\",\"FTRECEIVE\",\"FTLASTEDIT\",\"FCMAINJOBH\",\"FCSUBJOBH\",\"FCCREATEAP\",\"FCLID\",\"FNPRODAGE\",\"FCCOUNTER\",\"FCMORDERI\",\"FCRECALBY\" ,"
                     + "\"FTLASRECAL\",\"FCMNMGLH\",\"FCMNMGL\",\"FNPAYAMT\",\"FNPAYAMTKE\",\"FNWTAXRATE\",\"FNWTAXAMT\",\"FNWTAXAMTK\") VALUES (" 
             };
             /*INSERT INTO [similanwms_test].[dbo].[MAPTEST]
                          ([NAME])
                               VALUES
                                          ('Chatcharin');
                      INSERT INTO [similanwms_test].[dbo].[MAPCUS]
                            ([WMS_ID]
                                      ,[FCID])
                               VALUES
                            (SCOPE_IDENTITY(),'Chatch');
                            Select  SCOPE_IDENTITY()
              */
             
            
             
             if(num==0){
                queries[num] += tables[0].get(i)[0]+"','1','APPROVE','NULL','"+tables[0].get(i)[1]+"');";
                time[num] = tables[0].get(i)[1];
            }else if(num==1){
                queries[num] += "EUROFOOD','"+tables[1].get(i)[1]+"','"+tables[1].get(i)[2]+"','"+tables[1].get(i)[3]+"','"+tables[1].get(i)[4]+"','"+tables[1].get(i)[5]+"','"+tables[1].get(i)[4]+"','01','"+tables[1].get(i)[6]+"','"+tables[1].get(i)[7]+"','"+tables[1].get(i)[8]+"','"+tables[1].get(i)[9]+"','"+tables[1].get(i)[10]+"','"+tables[1].get(i)[11]+"','sy');";
                time[num] = tables[1].get(i)[6];
            }else if(num==2){
                queries[num] += "EUROFOOD','"+tables[2].get(i)[1]+"','ADHOC','"+tables[2].get(i)[3]+"','WAITING','"+tables[2].get(i)[5]+"','"+tables[2].get(i)[6]+"','"+tables[2].get(i)[7]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','"+tables[2].get(i)[8]+"','NULL','"+tables[2].get(i)[9]+"','0','"+tables[2].get(i)[10]+"','sy');";
                  time[num] = tables[2].get(i)[10];
            }else if(num==3){
                queries[num] += "EUROFOOD"+"','"+tables[3].get(i)[1]+"','"+tables[3].get(i)[2]+"','"+tables[3].get(i)[2]+"','"+tables[3].get(i)[4]
                        +"','"+tables[3].get(i)[4]+"','"+tables[3].get(i)[5]+"','"+tables[3].get(i)[6]+"',"+tables[3].get(i)[13]+","+tables[3].get(i)[13]
                        +","+tables[3].get(i)[14]+","+tables[3].get(i)[8]+","+tables[3].get(i)[9]+",'WAITING','WAITING',"
                        + "'01','UNKNOWN','UNKNOWN',NULL,'"+tables[3].get(i)[10]+"',"
                        + "'WAREHOUSE','"+tables[3].get(i)[12]+"','SIMILAN',NULL,NULL,'sy');";
                 time[num] = tables[3].get(i)[12];
            }else if(num==4){
                queries[num] += "EUROFOOD','"+tables[4].get(i)[1]+"','"+tables[4].get(i)[2]+"','"+tables[4].get(i)[3]+"','"+tables[4].get(i)[1]+"','"
                               +tables[4].get(i)[4]+"','"+tables[4].get(i)[5]+"','Y','ADHOC','NULL','"
                               +tables[4].get(i)[3]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]
                               +"','"+tables[4].get(i)[2]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]+"','"+tables[4].get(i)[6]
                               +"','"+tables[4].get(i)[6]+"','DATA ENTRY','DOCK11','"+tables[4].get(i)[8]+"','"+tables[4].get(i)[8]
                               +"','0','"+tables[4].get(i)[9]+"','sy');";     
                  time[num] = tables[4].get(i)[8];
            }else if(num==5){
                queries[num] += tables[5].get(i)[0]+"','"+tables[5].get(i)[0]+"','"+tables[5].get(i)[0]+"','"+tables[5].get(i)[1]+"','"+tables[5].get(i)[2]+"','"+tables[5].get(i)[3]+"','"+tables[5].get(i)[4]+"','"+tables[5].get(i)[5]+"','01','"+tables[5].get(i)[6]+"','"+tables[5].get(i)[6]+"','EURO WH','WARE_HOUSE','"+tables[5].get(i)[7]+"','sy');";
                  time[num] = tables[5].get(i)[6];
            }else if(num==6){
                 String address[] = tables[6].get(i)[3].split(" ");
                 p("--- length address [] :"+address.length+" String length : "+tables[6].get(i)[3].length());
                queries[num] += tables[6].get(i)[0]+"','"+tables[6].get(i)[1]+"','"+tables[6].get(i)[2]+"',NULL,'"+tables[6].get(i)[3]+"','"+tables[6].get(i)[3]+"','"+tables[6].get(i)[3]+"','"+tables[6].get(i)[3]+"','"+tables[6].get(i)[3]+"','"+tables[6].get(i)[4]+"','"+tables[6].get(i)[3]+"',NULL,'"+tables[6].get(i)[6]+"',NULL,NULL,'"+tables[6].get(i)[6]+"','"+tables[6].get(i)[7]+"','sy');";
                  time[num] = tables[6].get(i)[7];
            }else if(num==7){
                queries[num] += tables[7].get(i)[0]+"','"+tables[7].get(i)[1]+"','"+tables[7].get(i)[2]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"','"+tables[7].get(i)[3]+"',NULL,'APPROVE','NULL','"+tables[7].get(i)[4]+"','sy');";
                  time[num] = tables[7].get(i)[4];
            }else if(num==8){
                queries[num] += tables[8].get(i)[0]+"','"+tables[8].get(i)[1]+"','"+tables[8].get(i)[2]+"','"+tables[8].get(i)[3]+"','NULL','0','0','"+tables[8].get(i)[4]+"','01','0','"+tables[8].get(i)[5]+"','sy');"; 
                  time[num] = tables[8].get(i)[5];
            }else if(num==9){
                queries[num] += tables[9].get(i)[0]+"','"+tables[9].get(i)[1]+"','"+tables[9].get(i)[1]+"','"+tables[9].get(i)[2]+"','"+tables[9].get(i)[3]+"','"+tables[9].get(i)[4]+"','"+tables[9].get(i)[5]+"','NULL','NULL');";
                  time[num] = tables[9].get(i)[5];
            }else if(num==10){
                String fcskid = new_id("GLREF");
                String colum12 = "0";
                String fccode = getGLfccode("GLREF");
                p("finish create new id fccode :"+fccode);
                try{
                if(!tables[10].get(i)[12].isEmpty()&&isNumber(tables[10].get(i)[12]))
                  colum12  = tables[10].get(i)[12]; 
                  }catch(Exception e){p("chech colum new id");}
                p("create query GLREF");
                  try{
                        for(int ix=0;ix<tables[10].get(i).length;ix++)
                            if(tables[10].get(i)[ix]==null) tables[10].get(i)[ix] ="0";
                        
                    }catch(Exception e){p("error fix null");} 
                  
                       String values[] = {fcskid+"","''","''","'V1'","'S'","'SI'","'$%'","'$%'","'u?;((()'","'u?;?((()'",""
                              + "'u?;?((()'","''","'P'","''","''","''","'"+tables[10].get(i)[3]+"'","'"+tables[10].get(i)[4]+"'","'"+tables[10].get(i)[5]+"'","NULL",""
                              + "NULL","''","'$","'","'"+fccode+"'","'"+tables[10].get(i)[7]+"'","0","0","0","0","0","0"
                              + "",""+tables[10].get(i)[9]+"","0","0","0","0","'Y'","'1'",""+tables[10].get(i)[9]+"",""+tables[10].get(i)[9]+"","0"
                              + "","0","'"+tables[10].get(i)[10]+"'","'"+tables[10].get(i)[11]+"'",""+colum12+"","'N'","'Y'","'Y'"
                              + "","0","''","''"
                              + "","''","''","'$%'","'$%'","''","'"+getField("select top 1 FMMEMDATA From dbo.COOR where FCCODE='"+tables[10].get(i)[10]+"'")+"'","''"
                              + "","''","''","''",""
                             + "''","''","''","'E'","'N'","0","0","''","''","''","'"+tables[10].get(i)[4]+"'","0","''","''","''","''","''","''","''",""
                             + "''","''","''","'"+tables[10].get(i)[3]+"'","'u?;?((()'","''","''","1","0",""+tables[10].get(i)[9]+"",""+tables[10].get(i)[9]+"","0","0","''","''","''","'"+tables[10].get(i)[13]+"'","''","''","''","'"+tables[10].get(i)[14]+"'","''","'"+tables[10].get(i)[15]+"'","0","''","''","''","''","''","''","''","''","''","NULL","NULL","NULL","''","0","0","'"+tables[10].get(i)[3]+"'",""
                        + "0","0","NULL","''","''","''","''","''","''","''","NULL","''","'"+tables[10].get(i)[14]+"'","''","''","''","''","''","0","0","0","0","0","0","NULL","'V1'","0","''","''","''","''","''","''","''","0","'"+tables[10].get(i)[3]+"'","NULL","NULL","NULL","'"+tables[10].get(i)[0]+"'","'sy'","0","0","''","'u—ช (((/'","'"+tables[10].get(i)[14]+"'","''","''"};
                   String select = " \"FCSKID\",\"FCUDATE\",\"FCDATASER\",\"FCLUPDAPP\",\"FCRFTYPE\",\"FCREFTYPE\",\"FCCORP\",\"FCBRANCH\",\"FCDEPT\",\"FCSECT\","
                     + "\"FCJOB\",\"FCSTAT\",\"FCSTEP\",\"FCACSTEP\",\"FCGLHEAD\",\"FCGLHEADCA\",\"FDDATE\",\"FDRECEDATE\",\"FDDUEDATE\",\"FDLAYDATE\","
                     + "\"FDDEBTDATE\",\"FCDEBTCODE\",\"FCBOOK\",\"FCCODE\",\"FCREFNO\",\"FNDISCAMTI\",\"FNDISCAMT1\",\"FNDISCAMT2\",\"FNDISCAMTA\",\"FNDISCPCN1\","
                     + "\"FNDISCPCN2\",\"FNDISCPCN3\",\"FNAMT\",\"FNPAYAMT\",\"FNSPAYAMT\",\"FNBEFOAMT\",\"FCVATISOUT\",\"FCVATTYPE\","
                     + "\"FNVATRATE\",\"FNVATAMT\",\"FNAMT2\",\"FNAMTNOVAT\",\"FCCOOR\",\"FCEMPL\",\"FNCREDTERM\",\"FCISCASH\",\"FCHASRET\",\"FCVATDUE\","
                     + "\"FNDEBTZERO\",\"FCLAYH\",\"FCLAYSEQ\",\"FCFRWHOUSE\",\"FCTOWHOUSE\",\"FCCREATEBY\",\"FCCORRECTB\",\"FCCANCELBY\",\"FMMEMDATA\",\"FCSEQ\","
                     + "\"FCPROMOTE\",\"FCVATCOOR\",\"FCRECVMAN\",\"FCSTEPX1\",\"FCSTEPX2\",\"FCCREATETY\",\"FCEAFTERR\",\"FCATSTEP\",\"FNVATPORT\",\"FNVATPORTA\","
                     + "\"FCMACHINE\",\"FCPERIODS\",\"FCSELTAG\",\"FTDATETIME\",\"FIMILLISEC\",\"FCUTIME\",\"FCCASHIER\",\"FCTXQCBRAN\",\"FCTXQCWHOU\",\"FCTXQCBOOK\","
                     + "\"FCTXQCGLRE\",\"FCTXBRANCH\",\"FCTXWHOUSE\",\"FCTXBOOK\",\"FCAPPROVEB\",\"FDFULLFILL\",\"FCPROJ\",\"FCDISCSTR\",\"FCCURRENCY\",\"FNXRATE\",\"FNAMTKE\",\"FNVATAMTKE\"," 
                     + "\"FNPAYAMTKE\",\"FNDISCAMTK\",\"FNBPAYINV\",\"FCVATSEQ\",\"FCTIMESTAM\",\"FCTRANWHY\",\"FCDELICOOR\",\"FCSUBBR\",\"FCMORDERH\",\"FCPLANT\",\"FTLASTUPD\",\"FCXFERSTEP\",\"FTXFER\",\"FNSTOCKUPD\",\"FCDOCOWNER\",\"FCREFDO\",\"FCINVIA\"," 
                     + "\"FCOUTVIA\",\"FCVLDCTRL\",\"FCPCONTRAC\",\"FCQCMO\",\"FCQCMOBK\",\"FCHASCHAIN\",\"FMMEMDATA2\",\"FMMEMDATA3\",\"FMMEMDATA4\",\"FCSHOWPLED\",\"FNPPAYAMT\",\"FNPPAYAMTK\",\"FDVATDATE\",\"FNLOCKED\",\"FNRETENAMT\",\"FMMEMDATA5\",\"FCTXID\"," 
                     + "\"FCSUBTXID\",\"FCTRADETRM\",\"FCDELIEMPL\",\"FCLAYMETHD\",\"FCCONSIGN\",\"FCSTEPD\",\"FTRECEIVE\",\"FCLID\",\"FTLASTEDIT\",\"FCPAYTERM\",\"FCU1STATUS\",\"FCU2STATUS\",\"FCDTYPE1\",\"FCDTYPE2\",\"FNDEPAMT\",\"FNDEPAMTKE\",\"FNBEFDEP\"," 
                     + "\"FNBEFDEPKE\",\"FNU1CNT\",\"FNU2CNT\",\"FMERRMSG\",\"FCCREATEAP\",\"FNEXPAMT\",\"FCPERSON\",\"FCLINKAPP1\",\"FCLINKSTP1\",\"FCLINKAPP2\",\"FCLINKSTP2\",\"FCCOUNTER\",\"FCPDBRAND\",\"FNCRXRATE\",\"FDAPPROVE\",\"FMMEMDATA6\",\"FMMEMDATA7\",\"FMMEMDATA8\","
                     + "\"FMMEMDATA9\",\"FMMEMDATAA\",\"FNAFTDEP\",\"FNAFTDEPKE\",\"FCCHKDT\",\"FCRECALBY\",\"FTLASRECAL\",\"FCMNMGLH\",\"FCMNMGLHCA\" ";
                       
                       
                       dm.findcolum("GLREF",select, values);
                  
                       
                 queries[num] += fcskid+",'','','V1','S','SI','$%','$%','u?;((()','u?;?((()',"
                              + "'u?;?((()','','P','','','','"+tables[10].get(i)[3]+"','"+tables[10].get(i)[4]+"','"+tables[10].get(i)[5]+"',NULL,"
                              + "NULL,'','$,','"+fccode+"','"+tables[10].get(i)[7]+"',0,0,0,0,0,0"
                              + ","+tables[10].get(i)[9]+",0,0,0,0,'Y','1',"+tables[10].get(i)[9]+","+tables[10].get(i)[9]+",0"
                              + ",0,'"+tables[10].get(i)[10]+"','"+tables[10].get(i)[11]+"',"+colum12+",'N','Y','Y'"
                              + ",0,'',''"
                              + ",'','','$%','$%','','"+getField("select top 1 FMMEMDATA From dbo.COOR where FCCODE='"+tables[10].get(i)[10]+"'")+"',''"
                              + ",'','','',"
                             + "'','','','E','N',0,0,'','','','"+tables[10].get(i)[4]+"',0,'','','','','','','',"
                             + "'','','','"+tables[10].get(i)[3]+"','u?;?((()','','',1,0,"+tables[10].get(i)[9]+","+tables[10].get(i)[9]+",0,0,'','','','"+tables[10].get(i)[13]+"','','','','"+tables[10].get(i)[14]+"','','"+tables[10].get(i)[15]+"',0,'','','','','','','','','',NULL,NULL,NULL,'',0,0,'"+tables[10].get(i)[3]+"',"
                        + "0,0,NULL,'','','','','','','',NULL,'','"+tables[10].get(i)[14]+"','','','','','',0,0,0,0,0,0,NULL,'V1',0,'','','','','','','',0,'"+tables[10].get(i)[3]+"',NULL,NULL,NULL,'"+tables[10].get(i)[0]+"','sy',0,0,'','u—ช (((/','"+tables[10].get(i)[14]+"','','');";
              time[num] = tables[10].get(i)[3];
            }else if(num==11){
                String fcskid = new_id("REFPROD");
                 p("finish create new id");
                String deliver = tables[11].get(i)[15]==null ? tables[11].get(i)[14]:tables[11].get(i)[15];
                String fddate = tables[11].get(i)[4]==null?  deliver:tables[11].get(i)[4];
                // Time Emty
                if( tables[11].get(i)[4]==null)  tables[11].get(i)[4]   =  tables[11].get(i)[14];
                if( tables[11].get(i)[15]==null)  tables[11].get(i)[15] =  tables[11].get(i)[14];
                if( tables[11].get(i)[13]==null) tables[11].get(i)[13]  =  "0";
                
                String fcjob  = tables[11].get(i)[2];
                p("fcdate "+fddate);
                p(" finish assign fcjob "+fcjob);
       
                    long tmp = 0;
                    p("start fix fcjob");
                    if(fcjob !=null){
                    try{
                        tmp = Long.parseLong(fcjob);
                        fcjob = String.valueOf(tmp);
                    }catch(Exception e){
                        try{
                       p("error fix fcjob");
                        String t = fcjob;
                        fcjob = t.substring(0,7);
                        }catch(Exception ex){
                       p("error2 fix fcjob finish");
                        }
                    }
                    }
                    try{
                        for(int ix=0;ix<tables[11].get(i).length;ix++)
                            if(tables[11].get(i)[ix]==null) tables[11].get(i)[ix] ="0";
                        
                    }catch(Exception e){p("error fix null");} 
                    
                    
                    
                    
                queries[num] += fcskid+",'"+tables[11].get(i)[0]+"','','','',"
                        + "'V7','','"+tables[11].get(i)[1]+"','u?;((()','uคฌฦ(((*',"
                        + "'"+fcjob+"','','"+getField("select top 1 FCSKID from dbo.GLREF where FMMEMDATA9='"+tables[11].get(i)[3]+"'")+"','"+fddate+"','"+getField("select top 1 FCSKID From COOR where FCCODE='"+tables[11].get(i)[5]+"'")+"',"
                        + "'P','O','S','SI','1',"
                        + "'0','','','','"+getField("select top 1 FCSKID from dbo.PROD where FCCODE='"+tables[11].get(i)[6]+"'")+"',"
                        + "'"+tables[11].get(i)[7]+"','NULL',1,"+tables[11].get(i)[8]+",'NULL',"
                        + "'NULL',1,0,"+tables[11].get(i)[9]+",0,"
                        + "0,0,'',0,'',"
                        + "'',0,0,'"+tables[11].get(i)[11]+"','',"
                        + "'"+tables[11].get(i)[1]+"','"+tables[11].get(i)[12]+"','0','','',"
                        + "'','E','','"+tables[11].get(i)[4]+"','0',"
                        + "0,0,'',0,0,"
                        + ""+tables[11].get(i)[13]+",1,0,'NULL','NULL',"
                        + "'NULL','NULL','NULL','NULL','NULL','NULL','NULL','NULL','','','','','','','','','"+tables[11].get(i)[14]+"','','','','"+tables[11].get(i)[15]+"',"
                        + "'','"+tables[11].get(i)[15]+"','','',NULL,NULL,'','',0,'',0,'','','"+tables[11].get(i)[15]+"','','',NULL,'"+tables[11].get(i)[15]+"','','','V7','',0,'','','"+tables[11].get(i)[16]+"','"+tables[11].get(i)[14]+"','','sy',0,0,0,0,0);";
                 time[num] = tables[11].get(i)[4];
            } 

            System.out.println(queries[num]);
            
            try {
                if(num<10){
  
                Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                //     stmt.execute(queries[num]);
                  
                try{
                  stmt.execute(queries[num]);
                  updateLast(table[num],time[num]);
                }catch(Exception e){
                    String[] delete = new String[]{
                     "DELETE FROM \"approve_detail\" WHERE \"po_no\"='"+tables[num].get(i)[0]+"';",
                     "DELETE FROM \"order_lines\" WHERE \"item_no\"='"+tables[num].get(i)[2]+"';",
                     "DELETE FROM \"purchase_orders\" WHERE \"supplier_name\"='"+tables[num].get(i)[6]+"';",
                     "DELETE FROM \"sales_details\" WHERE \"reference_no\"='"+tables[num].get(i)[2]+"';",
                     "DELETE FROM \"sales_orders\" WHERE \"order_no\"='"+tables[num].get(i)[1]+"';",
                     "DELETE FROM \"delivery_detail\" WHERE \"con_note\"='"+tables[num].get(i)[0]+"';",
                     "DELETE FROM \"customer_master\" WHERE \"customer\"='"+tables[num].get(i)[1]+"';",
                     "DELETE FROM \"suppliers\" WHERE \"supplier\"='"+tables[num].get(i)[1]+"';",
                     "DELETE FROM \"product_master\" WHERE \"item_no\"='"+tables[num].get(i)[1]+"';",
                     "DELETE FROM \"product_barcode\" WHERE \"item_no\"='"+tables[num].get(i)[1]+"';",
                     "DELETE FROM \"GLREF\" WHERE \"item_no\"='"+tables[num].get(i)[0]+"';",
                     "DELETE FROM \"REFPROD\" WHERE \"FCGLREF\"='"+tables[num].get(i)[11]+"';"
                    };
                    System.out.println("-- ER --"+e.getMessage()+"Rollback"+delete[num]);
                    System.out.println(getSQL(system[num],table[num],queries[num],e.getMessage()));
                    Statement e_stmt = mycon.createStatement();
                    e_stmt.execute(getSQL(system[num],table[num],queries[num],e.getMessage()));
                    e_stmt.close();
                   // stmt.execute(delete[num]);
                }
       
                stmt.close();
                }else{
                     Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                        try{
                                stmt.execute(queries[num]);
                                updateLast(table[num],time[num]);
                                p("start insert REPRO"+tables[num].size());
                                p("Finish Ref");
                      }catch(Exception e){
                                 String[] delete = new String[]{
                                 "DELETE FROM \"approve_detail\" WHERE \"po_no\"='"+tables[num].get(i)[0]+"';",
                                 "DELETE FROM \"order_lines\" WHERE \"item_no\"='"+tables[num].get(i)[2]+"';",
                                 "DELETE FROM \"purchase_orders\" WHERE \"supplier_name\"='"+tables[num].get(i)[6]+"';",
                                 "DELETE FROM \"sales_details\" WHERE \"item_no\"='"+tables[num].get(i)[5]+"';",
                                 "DELETE FROM \"sales_orders\" WHERE \"order_no\"='"+tables[num].get(i)[1]+"';",
                                 "DELETE FROM \"delivery_detail\" WHERE \"con_note\"='"+tables[num].get(i)[0]+"';",
                                 "DELETE FROM \"customer_master\" WHERE \"customer\"='"+tables[num].get(i)[1]+"';",
                                 "DELETE FROM \"suppliers\" WHERE \"supplier\"='"+tables[num].get(i)[1]+"';",
                                 "DELETE FROM \"product_master\" WHERE \"item_no\"='"+tables[num].get(i)[1]+"';",
                                 "DELETE FROM \"product_barcode\" WHERE \"item_no\"='"+tables[num].get(i)[1]+"';",
                                 "DELETE FROM \"GLREF\" WHERE \"FCSKID\"='"+tables[num].get(i)[0]+"';",
                                 "DELETE FROM \"REFPROD\" WHERE \"FCGLREF\"='"+tables[num].get(i)[11]+"';"

                                };
                                System.out.println("-- ER --"+e.getMessage()+"Rollback"+delete[num]);
                                System.out.println(getSQL(system[num],table[num],queries[num],e.getMessage()));
                                Statement e_stmt = mycon.createStatement();
                                // ) 
                                ResultSet rs=null;
                                try{
                                    rs= e_stmt.executeQuery("SELECT * FROM `map_data`.`data_log` nx WHERE nx.sql_txt ='"+ReSymbol(queries[num])+"'");
                                }catch(Exception sx){}
                                if(rs ==null || (!rs.next()))
                                    e_stmt.execute(getSQL(system[num],table[num],queries[num],e.getMessage()));
                                e_stmt.close();
                     }
               // stmt.close();
                }
            } catch (Exception e) {
                 try {
                     System.out.println(e);
                     Statement e_stmt = mycon.createStatement();
                     e_stmt.execute(getSQL(system[num],table[num],queries[num],e.getMessage()));
                    
                     e_stmt.close();
                     msg = "Connec tion failed !";
                   //  error = true;
                 } catch (SQLException ex) {
                     Logger.getLogger(MSSQL_Connect.class.getName()).log(Level.SEVERE, null, ex);
                 }
            }
        }
    }
    public String getField(String sql){
        String field = "";
        try{
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(sql);
            if(rs.next()){
                 field =  rs.getString(1);
            }
            //rs.close(); stmt.close();
            
        }catch(Exception e){p("error get field");}
        return field;
    }
    public String getIDFormula(String value,String table){
        String code = "";
        try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
          //  con = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA","sa","P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
           // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select top 1 dbo."+table+".FCID  from  dbo."+table+" where dbo."+table+".WMS_ID="+value);
            // ResultSetMetaData rsMetaData = rs.getMetaData();
            //int column = rsMetaData.getColumnCount();
            if(rs.next()){
                    code = rs.getString(2);
                    System.out.println("First :"+code);
            }
             //rs.close();stmt.close();
             return "0x"+code;
             
         } catch (Exception e) {
            System.out.println(e);
            return "0x"+code;
         }

    }
      SimpleDateFormat sdf = new SimpleDateFormat("yyMMHHmmssSS");
   public String getGLfccode(String table){
       return sdf.format(new java.util.Date());
    }
    
    //create startup registry
    void createRegistry(){
        try {
            msg = "Creating starts up point";

            //find current location
            URL location = getClass().getProtectionDomain().getCodeSource().getLocation();
            String path = (location.getPath().replace("%20"," ")).replace("/","\\");
            path = path.substring(1,path.length());

            Process cmd = Runtime.getRuntime().exec("reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run /v DataSynchro /t REG_SZ /d \""+path+"\"");
            OutputStreamWriter osw = new OutputStreamWriter(cmd.getOutputStream());
            osw.write("y");
            osw.flush();
            osw.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(cmd.getInputStream()));

            String line = "";
            while((line = in.readLine())!=null){
                System.out.println(line);
            }
            in.close();
        } catch (IOException e) {
                e.printStackTrace();
        }
    }

        public String new_id(String table){
            int i =0;
            int j =0;
            String new_id="0x";
            String code="";
            Connection cons;
        try {
             Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
           // cons = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA","sa","P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
           // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select  CAST(MAX(dbo."+table+".FCSKID) AS binary(8)) from  dbo."+table+" ");
            p("select CAST(MAX(dbo."+table+".FCSKID) AS binary(8))  from  dbo."+table+" ");
            // ResultSetMetaData rsMetaData = rs.getMetaData();
            //int column = rsMetaData.getColumnCount();
            if(rs.next()){
               code = rs.getString(1);
               System.out.println("First :"+code);
            }
           // code ="FFFFFFFFFFFFFFFF";
            System.out.println("Test :"+code);
            String tmp ="";
            String work="";
           for(int a=0;a<8;a++){
               work = code.substring(16-((a+1)*2), 16-(a*2));
               tmp  = code; 
               int    hex  = Integer.decode("#"+work);
               int    next = 0;
               if(getValue(hex)!=207){
                   next = (getValue(hex)+1);
               }
             
              System.out.println(" new id : "+getHEX(next)+" : "+Integer.toHexString(getHEX(next)).toUpperCase());
              if(a==0)
               code = tmp.substring(0,16-((a+1)*2)) +Integer.toHexString(getHEX(next)).toUpperCase();
              else{
                  code = tmp.substring(0,16-((a+1)*2)) +Integer.toHexString(getHEX(next)).toUpperCase()+tmp.substring(16-((a)*2),16);
                  System.out.println("1 :"+ tmp.substring(0,16-((a+1)*2)));
                  System.out.println("2 :"+ Integer.toHexString(getHEX(next)).toUpperCase());
                  System.out.println("3 :"+ tmp.substring(16-((a)*2),16));
              }
              System.out.println("HEX Ox"+code);
              if(next!=0) break;
           }
           
          /*  while(rs.next()){
                String s = new String(rs.getString(1).getBytes(),"UTF-8");
                String codes = rs.getString(2).substring(14, 16);
              
                if(i>621 && i< 830){
                     //System.out.println(" "+(j++)+":"+s.codePointAt(7)+" HEX :"+code+" DEC :"+Long.decode("#"+code)+" : "+rs.getString(2));
                     System.out.println("case "+Integer.decode("#"+code)+":return "+(j++)+";");    
                }
                  i++;
             }*/
               
          // long a = Long.decode("0x75B0753A2828282A00000000000000000000000000000000000000000000");
          // System.out.println(""+a);
           // rs.close();  stmt.close();   cons.close();
             return "0x"+code; 
            
         } catch (Exception e) {
            System.out.println("create id error :"+e);
            return "0x"+code;
         }
    } 
    
    
    String pk[]= {"(",")","*","+",",","-",".","/","0","1","2","3","4","5",")","7","8","9",":",";","<","=",">","?","@","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","[","\\","]","^","_","`","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","{","|","}","~","","�","�","�","�","…","�","�","�","�","�","�","�","�","�","�","�","‘","’","“","”","•","–","—","�","�","�","�","�","�","�","�"," ","ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ","ญ","ฎ","ฏ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ","น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ฤ","ล","ฦ","ว","ศ","ษ","ส","ห","ฬ","อ","ฮ","ะ","ั","า","ำ","ิ","ี","ึ","ื","ุ","ู","ฺ","�","�","�","�","฿","เ","แ","โ","ใ","ไ","ๅ","ๆ","ํ","๎","๏","๐","๑","๒","๓","๔","๕","๖","๗","๘","๙","๚","๛","�","�","�","�"};
    public int getHEX(int c){
        switch(c){
            case 0:return 0x28;
            case 1:return 0x29;
            case 2:return 0x2A;
            case 3:return 0x2B;
            case 4:return 0x2C;
            case 5:return 0x2D;
            case 6:return 0x2E;
            case 7:return 0x2F;
            case 8:return 0x30;
            case 9:return 0x31;
            case 10:return 0x32;
            case 11:return 0x33;
            case 12:return 0x34;
            case 13:return 0x35;
            case 14:return 0x36;
            case 15:return 0x37;
            case 16:return 0x38;
            case 17:return 0x39;
            case 18:return 0x3A;
            case 19:return 0x3B;
            case 20:return 0x3C;
            case 21:return 0x3D;
            case 22:return 0x3E;
            case 23:return 0x3F;
            case 24:return 0x40;
            case 25:return 0x41;
            case 26:return 0x42;
            case 27:return 0x43;
            case 28:return 0x44;
            case 29:return 0x45;
            case 30:return 0x46;
            case 31:return 0x47;
            case 32:return 0x48;
            case 33:return 0x49;
            case 34:return 0x4A;
            case 35:return 0x4B;
            case 36:return 0x4C;
            case 37:return 0x4D;
            case 38:return 0x4E;
            case 39:return 0x4F;
            case 40:return 0x50;
            case 41:return 0x51;
            case 42:return 0x52;
            case 43:return 0x53;
            case 44:return 0x54;
            case 45:return 0x55;
            case 46:return 0x56;
            case 47:return 0x57;
            case 48:return 0x58;
            case 49:return 0x59;
            case 50:return 0x5A;
            case 51:return 0x5B;
            case 52:return 0x5C;
            case 53:return 0x5D;
            case 54:return 0x5E;
            case 55:return 0x5F;
            case 56:return 0x60;
            case 57:return 0x61;
            case 58:return 0x62;
            case 59:return 0x63;
            case 60:return 0x64;
            case 61:return 0x65;
            case 62:return 0x66;
            case 63:return 0x67;
            case 64:return 0x68;
            case 65:return 0x69;
            case 66:return 0x6A;
            case 67:return 0x6B;
            case 68:return 0x6C;
            case 69:return 0x6D;
            case 70:return 0x6E;
            case 71:return 0x6F;
            case 72:return 0x70;
            case 73:return 0x71;
            case 74:return 0x72;
            case 75:return 0x73;
            case 76:return 0x74;
            case 77:return 0x75;
            case 78:return 0x76;
            case 79:return 0x77;
            case 80:return 0x78;
            case 81:return 0x79;
            case 82:return 0x7A;
            case 83:return 0x7B;
            case 84:return 0x7C;
            case 85:return 0x7D;
            case 86:return 0x7E;
            case 87:return 0x7F;
            case 88:return 0x81;
            case 89:return 0x82;
            case 90:return 0x83;
            case 91:return 0x84;
            case 92:return 0x85;
            case 93:return 0x86;
            case 94:return 0x87;
            case 95:return 0x88;
            case 96:return 0x89;
            case 97:return 0x8A;
            case 98:return 0x8B;
            case 99:return 0x8C;
            case 100:return 0x8D;
            case 101:return 0x8E;
            case 102:return 0x8F;
            case 103:return 0x90;
            case 104:return 0x91;
            case 105:return 0x92;
            case 106:return 0x93;
            case 107:return 0x94;
            case 108:return 0x95;
            case 109:return 0x96;
            case 110:return 0x97;
            case 111:return 0x98;
            case 112:return 0x99;
            case 113:return 0x9A;
            case 114:return 0x9B;
            case 115:return 0x9C;
            case 116:return 0x9D;
            case 117:return 0x9E;
            case 118:return 0x9F;
            case 119:return 0xA0;
            case 120:return 0xA1;
            case 121:return 0xA2;
            case 122:return 0xA3;
            case 123:return 0xA4;
            case 124:return 0xA5;
            case 125:return 0xA6;
            case 126:return 0xA7;
            case 127:return 0xA8;
            case 128:return 0xA9;
            case 129:return 0xAA;
            case 130:return 0xAB;
            case 131:return 0xAC;
            case 132:return 0xAD;
            case 133:return 0xAE;
            case 134:return 0xAF;
            case 135:return 0xB0;
            case 136:return 0xB1;
            case 137:return 0xB2;
            case 138:return 0xB3;
            case 139:return 0xB4;
            case 140:return 0xB5;
            case 141:return 0xB6;
            case 142:return 0xB7;
            case 143:return 0xB8;
            case 144:return 0xB9;
            case 145:return 0xBA;
            case 146:return 0xBB;
            case 147:return 0xBC;
            case 148:return 0xBD;
            case 149:return 0xBE;
            case 150:return 0xBF;
            case 151:return 0xC0;
            case 152:return 0xC1;
            case 153:return 0xC2;
            case 154:return 0xC3;
            case 155:return 0xC4;
            case 156:return 0xC5;
            case 157:return 0xC6;
            case 158:return 0xC7;
            case 159:return 0xC8;
            case 160:return 0xC9;
            case 161:return 0xCA;
            case 162:return 0xCB;
            case 163:return 0xCC;
            case 164:return 0xCD;
            case 165:return 0xCE;
            case 166:return 0xD0;
            case 167:return 0xD1;
            case 168:return 0xD2;
            case 169:return 0xD3;
            case 170:return 0xD4;
            case 171:return 0xD5;
            case 172:return 0xD6;
            case 173:return 0xD7;
            case 174:return 0xD8;
            case 175:return 0xD9;
            case 176:return 0xDA;
            case 177:return 0xDB;
            case 178:return 0xDC;
            case 179:return 0xDD;
            case 180:return 0xDE;
            case 181:return 0xDF;
            case 182:return 0xE0;
            case 183:return 0xE1;
            case 184:return 0xE2;
            case 185:return 0xE3;
            case 186:return 0xE4;
            case 187:return 0xE5;
            case 188:return 0xE6;
            case 189:return 0xED;
            case 190:return 0xEE;
            case 191:return 0xEF;
            case 192:return 0xF0;
            case 193:return 0xF1;
            case 194:return 0xF2;
            case 195:return 0xF3;
            case 196:return 0xF4;
            case 197:return 0xF5;
            case 198:return 0xF6;
            case 199:return 0xF7;
            case 200:return 0xF8;
            case 201:return 0xF9;
            case 202:return 0xFA;
            case 203:return 0xFB;
            case 204:return 0xFC;
            case 205:return 0xFD;
            case 206:return 0xFE;
            case 207:return 0xFF;         
        }
        return -1;
    }
    
    public int getValue(int x){
        switch(x){
            case 40:return 0;
            case 41:return 1;
            case 42:return 2;
            case 43:return 3;
            case 44:return 4;
            case 45:return 5;
            case 46:return 6;
            case 47:return 7;
            case 48:return 8;
            case 49:return 9;
            case 50:return 10;
            case 51:return 11;
            case 52:return 12;
            case 53:return 13;
            case 54:return 14;
            case 55:return 15;
            case 56:return 16;
            case 57:return 17;
            case 58:return 18;
            case 59:return 19;
            case 60:return 20;
            case 61:return 21;
            case 62:return 22;
            case 63:return 23;
            case 64:return 24;
            case 65:return 25;
            case 66:return 26;
            case 67:return 27;
            case 68:return 28;
            case 69:return 29;
            case 70:return 30;
            case 71:return 31;
            case 72:return 32;
            case 73:return 33;
            case 74:return 34;
            case 75:return 35;
            case 76:return 36;
            case 77:return 37;
            case 78:return 38;
            case 79:return 39;
            case 80:return 40;
            case 81:return 41;
            case 82:return 42;
            case 83:return 43;
            case 84:return 44;
            case 85:return 45;
            case 86:return 46;
            case 87:return 47;
            case 88:return 48;
            case 89:return 49;
            case 90:return 50;
            case 91:return 51;
            case 92:return 52;
            case 93:return 53;
            case 94:return 54;
            case 95:return 55;
            case 96:return 56;
            case 97:return 57;
            case 98:return 58;
            case 99:return 59;
            case 100:return 60;
            case 101:return 61;
            case 102:return 62;
            case 103:return 63;
            case 104:return 64;
            case 105:return 65;
            case 106:return 66;
            case 107:return 67;
            case 108:return 68;
            case 109:return 69;
            case 110:return 70;
            case 111:return 71;
            case 112:return 72;
            case 113:return 73;
            case 114:return 74;
            case 115:return 75;
            case 116:return 76;
            case 117:return 77;
            case 118:return 78;
            case 119:return 79;
            case 120:return 80;
            case 121:return 81;
            case 122:return 82;
            case 123:return 83;
            case 124:return 84;
            case 125:return 85;
            case 126:return 86;
            case 127:return 87;
            case 129:return 88;
            case 130:return 89;
            case 131:return 90;
            case 132:return 91;
            case 133:return 92;
            case 134:return 93;
            case 135:return 94;
            case 136:return 95;
            case 137:return 96;
            case 138:return 97;
            case 139:return 98;
            case 140:return 99;
            case 141:return 100;
            case 142:return 101;
            case 143:return 102;
            case 144:return 103;
            case 145:return 104;
            case 146:return 105;
            case 147:return 106;
            case 148:return 107;
            case 149:return 108;
            case 150:return 109;
            case 151:return 110;
            case 152:return 111;
            case 153:return 112;
            case 154:return 113;
            case 155:return 114;
            case 156:return 115;
            case 157:return 116;
            case 158:return 117;
            case 159:return 118;
            case 160:return 119;
            case 161:return 120;
            case 162:return 121;
            case 163:return 122;
            case 164:return 123;
            case 165:return 124;
            case 166:return 125;
            case 167:return 126;
            case 168:return 127;
            case 169:return 128;
            case 170:return 129;
            case 171:return 130;
            case 172:return 131;
            case 173:return 132;
            case 174:return 133;
            case 175:return 134;
            case 176:return 135;
            case 177:return 136;
            case 178:return 137;
            case 179:return 138;
            case 180:return 139;
            case 181:return 140;
            case 182:return 141;
            case 183:return 142;
            case 184:return 143;
            case 185:return 144;
            case 186:return 145;
            case 187:return 146;
            case 188:return 147;
            case 189:return 148;
            case 190:return 149;
            case 191:return 150;
            case 192:return 151;
            case 193:return 152;
            case 194:return 153;
            case 195:return 154;
            case 196:return 155;
            case 197:return 156;
            case 198:return 157;
            case 199:return 158;
            case 200:return 159;
            case 201:return 160;
            case 202:return 161;
            case 203:return 162;
            case 204:return 163;
            case 205:return 164;
            case 206:return 165;
            case 208:return 166;
            case 209:return 167;
            case 210:return 168;
            case 211:return 169;
            case 212:return 170;
            case 213:return 171;
            case 214:return 172;
            case 215:return 173;
            case 216:return 174;
            case 217:return 175;
            case 218:return 176;
            case 219:return 177;
            case 220:return 178;
            case 221:return 179;
            case 222:return 180;
            case 223:return 181;
            case 224:return 182;
            case 225:return 183;
            case 226:return 184;
            case 227:return 185;
            case 228:return 186;
            case 229:return 187;
            case 230:return 188;
            case 237:return 189;
            case 238:return 190;
            case 239:return 191;
            case 240:return 192;
            case 241:return 193;
            case 242:return 194;
            case 243:return 195;
            case 244:return 196;
            case 245:return 197;
            case 246:return 198;
            case 247:return 199;
            case 248:return 200;
            case 249:return 201;
            case 250:return 202;
            case 251:return 203;
            case 252:return 204;
            case 253:return 205;
            case 254:return 206;
            case 255:return 207;
        }
        return 0;
    }
}