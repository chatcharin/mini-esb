/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mailverifier;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import static mailverifier.getTable.getSqlTypeName;

/**
 *
 * @author Aek
 */
public class DataManagement {

    public DataManagement(String path) {
        log("start read csv "+path);
         debug = false;
        readCSVtoData(path);
    }
    public DataManagement(){
        debug = false;
    }
    String path;
    private static final Logger LOG = Logger.getLogger(DataManagement.class.getName());

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }
    
    boolean debug = true;
    ArrayList<Data> data = new ArrayList<Data>();
    String dtable = null;
    String stable = null;
    String join   = "null";
    String where  = "null";
    String sys    = null;
    String field_time = null;
    String field_last = null;
    public HashMap id_table = new HashMap();
    int size = 0;
    ArrayList<String> multisource = new ArrayList<String>();
    class Data{
        String sourcetable;
        String destination;
        String defaultvalue;
        String type;
        int length;
        String sourcefield;
        String destinfield;
        String source;
        String data_default;
        String data_field;

        
        int i=0;
        int fml=0;
        int wms=0;

        public int getI() {
            return i;
        }

        public void setI(int i) {
            this.i = i;
        }

        public int getFml() {
            return fml;
        }

        public void setFml(int fml) {
            this.fml = fml;
        }

        public int getWms() {
            return wms;
        }

        public void setWms(int wms) {
            this.wms = wms;
        }

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }
        
        public String getData_default() {
            return data_default;
        }

        public void setData_default(String data_default) {
            this.data_default = data_default;
        }

        public String getData_field() {
            return data_field;
        }

        public void setData_field(String data_field) {
            this.data_field = data_field;
        }
        
        //                   orders_lines,         discount,   nvarchar,         50,  ORDERI.FNDISCAMTK, 
        public Data(String destination,String destinfield,String type, int length, String source,String sourcefield,String defaultvalue) {
          //  this.sourcetable = sourcetable;
            this.destination = destination;
            this.defaultvalue = defaultvalue;
            this.type = type;
            this.length = length;
            try{
               String va_arr[] = source.split(";");
               this.source = va_arr[0];
            }catch(Exception e)
            { 
                System.out.println("Data contrcter");
                this.source = "null";
            }
            
            this.sourcefield = getNull(sourcefield);
            this.destinfield = getNull(destinfield);
        }

        public String getDefaultvalue() {
            return defaultvalue;
        }
        public String getNull(String var){
            String tmp = var.trim();
            if(tmp.length()>0) return var;
            else return "null";
        }
        public void setDefaultvalue(String defaultvalue) {
            this.defaultvalue = defaultvalue;
        }

        public String getSourcefield() {
            return sourcefield;
        }

        public void setSourcefield(String sourcefield) {
            this.sourcefield = sourcefield;
        }

        public String getDestinfield() {
            return destinfield;
        }

        public void setDestinfield(String destinfield) {
            this.destinfield = destinfield;
        }
        
        public String getSourcetable() {
            return sourcetable;
        }

        public void setSourcetable(String sourcetable) {
            this.sourcetable = sourcetable;
        }

        public String getDestination() {
            return destination;
        }

        public void setDestination(String destination) {
            this.destination = destination;
        }



        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }
        
    }
    public String FixtoComma(String fix){
          String getString[] = fix.split("\"");
                    String new_str = "";
                    boolean replace = false;
                    if(getString.length>1){
                        replace = true;
                       for(int x=0;x<getString.length;x++)
                          if(x==1)
                           new_str += getString[x].replaceAll(",","comma")+", ";
                          else  new_str += getString[x]+", ";
                    }
           return new_str;
    }
    public String getComma(String fix){
        return fix.replaceAll("comma",",");
    }
    public void readCSVtoData(String pathfile){
        String trace = "start ...";
        LOG.fine("read csv");
        try{
                 
                  // Open the file that is the first 
                  // command line parameter
                  // "C:\\report\\mailtest.csv"
                  FileInputStream fstream = new FileInputStream(pathfile);
                  // Get the object of DataInputStream
                  DataInputStream in = new DataInputStream(fstream);
                  BufferedReader br = new BufferedReader(new InputStreamReader(in));
                  String strLine;
                  String leng;
                  int leng_int = 1;
                  trace = "init resource ..";
                  //Read File Line By Line
                  int i = 0;
                   br.readLine();// Read name table
                 if((strLine = br.readLine()) != null){
                     String value[] = strLine.split(",");
                     
                     String val[]= value[0].split(";");
                     sys        = val[0]; // Source System
                     dtable     = val[1]; // Destination Table
                     if(val.length >2) field_time = val[2]; // field name last time destination
                     if(val.length >3) field_last = val[3]; // field name last time source
                     log("destination :"+dtable);
                     stable = value[1];// mode
                     log("mode        :"+stable);
                     leng   = value[2]; // length
                     log("length      :"+leng);
                     try{
                         leng_int = Integer.parseInt(leng);
                     }catch(Exception e){
                         log("Error cast leng mode .. line 208");
                         leng_int = 1;
                     }
                     try{
                     for(int l=0;l<leng_int;l++){
                         
                       //  String va_arr[] = value[3+1].split(";");
                         multisource.add(value[3+l]);
                         log("source table:"+value[3+l]);
                     }
                     }catch(Exception e){
                         log("Error add multisource line 214 ");
                     }
                            
                     strLine="";
                  }
                 br.readLine();
                  trace = "load first line";
                  int length = 0;
                  while ((strLine = br.readLine()) != null){
                    String value[] = strLine.split(",");
                    log("length :"+value.length);
                    // , destination,  defaultvalue,  type,  length,sourcetable,  sourcefield,  destinfield
                    log("data add "+value[0]+" "+value[1]+" "+value[2]+" "+value[3]+" "+value[4]+" "+value[4]+" "+value[5]);
                   
                    String qstr = value[4].trim();
                    int lent = qstr.length();
                    //  destination, destinfield,type,length, source,sourcefield,defaultvalue
                    Data d = new Data(value[0],value[0], value[1],Fixlength(value[2]),value[3],value[4], value[5]);
                    d.setI(length);
                    length++;
                    data.add(d);
                    strLine = "";
                  }
                  size = length;
                  trace = "load multi line";
                  br.close();
                  in.close();
                  fstream.close();
           }catch(Exception e){
                 log("read csv error ..."+trace);
                 log(e.getMessage());
           }
    }
    
    public int Fixlength(String var){
        try {
            return Integer.parseInt(var);
        }catch(Exception e){
            return 23;
        }
    }
   public int convertType(String type){
       if(type.equalsIgnoreCase("decimal"))
             return 0;
       else  if(type.equalsIgnoreCase("char"))
             return 1;
       else  if(type.equalsIgnoreCase("datetime"))
             return 2;
       else  if(type.equalsIgnoreCase("varchar"))
             return 1;
       else  if(type.equalsIgnoreCase("int"))
             return 0;
       else  if(type.equalsIgnoreCase("text"))
             return 1;
       else  if(type.equalsIgnoreCase("NULL"))
             return 4;
       else  if(type.equalsIgnoreCase("null"))
             return 4;
       else  if(type.equalsIgnoreCase("numeric"))
             return 0;
       else  if(type.equalsIgnoreCase("bigint"))
             return 0;
       else  if(type.equalsIgnoreCase("nvarchar"))
             return 1;
       else  if(type.equalsIgnoreCase("id"))
             return 5;
       else return 1;
   }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");

    public String getGLfccode(String table) {
        return sdf.format(new java.util.Date());
    }
    public boolean isNumber(String number){
         if (number.matches("((-|\\+)?[0-9]+(\\.[0-9]+)?)+")) {  
          //  System.out.println("Is a number");
            return true;
        } else {  
        //    System.out.println("Is not a number");
            return false;
        }  
    }
   private String setValue(String o,String type,int length,String field,String table,String def){
      
       log("Field :"+field+"  data:"+o);
       try{
       int t = convertType(type);

       switch(t){
           /* Number */
           case 0:  String number = "";
                    if(checkNull(o)) number = def.equalsIgnoreCase("null")? "0":def;//
                    else             number = o;
                    if(!isNumber(number)){
                        if(field.equalsIgnoreCase("line_no"))
                            number = ""+getLineNumber(new_order_no);
                        else number = "0";
                    }  
                     
                    return number;
           /* String */
           case 1:  if(checkNull(o)) return def.equalsIgnoreCase("null")? "''":"'"+def+"'";//
                    else             return "'"+setLength(o, length, table, field)+"'";
           /* Date  */
           case 2: String time="";
                   if(checkNull(o)) time= def.equalsIgnoreCase("null")? "'"+sdf.format(new Date())+"'":"'"+def+"'";//
                   else            time= "'"+o+"'";
                   if(time.equalsIgnoreCase("'null'")) return "'"+sdf.format(new Date())+"'";
                   else return time;
           case 3: return "";
           case 4: return "";
           case 5: return "0x"+o;
           default: return  "";
        }
       }catch(Exception e){
           log("Error Class DataManagement method setValue "+e.getMessage());
           return "";
       }
    }
    private boolean checkNull(String o){
        String t= "'"+o+"'";
        return t.equalsIgnoreCase("'null'");
    }
    
    private String  setLength(String value,int length ,String table,String field)
    {
        if(value.length()<= length) return value;
        else{
            log("Table : "+table+" Field : "+field+" length= "+length+" value over length = "+value.length());
            return value.substring(0, length-1);
        }
    }
    
    private void log(String txt){
     //  if(debug)
       System.out.println("["+new Date().toString()+"] : "+txt);
    }
    ArrayList<Integer> wms_index= new ArrayList<Integer>();
    ArrayList<Integer> fml_index= new ArrayList<Integer>();
    
    public String getWhereTime(String field,String time){
        return " and ("+field+" >CONVERT(DateTime,'"+time.trim()+"')) ORDER BY "+field+" ASC ";
    }
    
    public String getSelect(String source,String tjw,String time){
       String sql="select  ";
       int z = data.size();
       int run = 0;
       if(source.equalsIgnoreCase("FML"))
             fml_index.clear();
       else  wms_index.clear();
       for(int i=0;i<z;i++){
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
              // log(" "+i+" :"+sql);
              // Filter by source
               if(data.get(i).source.equalsIgnoreCase(source)){
                  sql += data.get(i).sourcefield+",";
                  if(source.equalsIgnoreCase("FML"))
                      fml_index.add(new Integer(i));
                  else  wms_index.add(new Integer(i));
                  run++;
               }
           }
       }
       String tmp = sql;
     
       sql = tmp.substring(0,tmp.length()-1);
       
       sql += " from "+tjw;
       if(!field_last.isEmpty() && !time.equalsIgnoreCase("null")) 
           sql += getWhereTime(field_last, time);
       log("SQL = "+sql);
       return sql;
    }
       public String getSelect(String source,String tjw,String time,String before){
       String sql="select  ";
       int z = data.size();
       int run = 0;
       if(source.equalsIgnoreCase("FML"))
             fml_index.clear();
       else  wms_index.clear();
       for(int i=0;i<z;i++){
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
              // log(" "+i+" :"+sql);
              // Filter by source
               if(data.get(i).source.equalsIgnoreCase(source)){
                  sql += data.get(i).sourcefield+",";
                  if(source.equalsIgnoreCase("FML"))
                      fml_index.add(new Integer(i));
                  else  wms_index.add(new Integer(i));
                  run++;
               }
           }
       }
       String tmp = sql;
     
       sql = tmp.substring(0,tmp.length()-1);
       
       sql += before+" from "+tjw;
       if(!field_last.isEmpty() && !time.equalsIgnoreCase("null")) 
           sql += getWhereTime(field_last, time);
       log("SQL = "+sql);
       return sql;
    }

    public String createInsert(String val[]){
        log("-----1");
       String sql="insert into "+dtable+"(";
          int z = data.size();
          log("-----2");
       for(int i=0;i<z;i++){
           if(!data.get(i).destinfield.equalsIgnoreCase("null")){
               sql += data.get(i).destinfield+",";
           }
       }
       log("-----3");
       String tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+") values(";
       int j=0;
       log("-----4");
       for(int i=0;i<z;i++){
          // log("---- i : "+i);
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
               sql += setValue(val[j++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }else{
               sql += setValue(data.get(i).defaultvalue, data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }
       } 
       log("-----5");
       tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+");";
       log("-----6");
       return sql;
    }
    String new_order_no="";
    String order_no = "";
    int line_no = 1;
    public int getLineNumber(String order){
        if(order_no.equalsIgnoreCase(order))
            line_no++;
        else{
            order_no = order;
            line_no = 1;
         }  
        return line_no;
    }
    public String createInsertLast(String val[]){
        log("-----1");
       String sql="insert into "+dtable+"(";
          int z = data.size();
          log("-----2");
       for(int i=0;i<z;i++){
           if((!data.get(i).sourcefield.equalsIgnoreCase("null"))||(!data.get(i).defaultvalue.equalsIgnoreCase("null"))){
               sql += data.get(i).destinfield+",";
           }
       }
       log("-----3");
       String tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+") values(";
       int j=0;
       log("-----4");
       for(int i=0;i<z;i++){
          // log("---- i : "+i);
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
              if(data.get(i).destinfield.equalsIgnoreCase("line_no"))
                   new_order_no = val[j-1];
               sql += setValue(val[j++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }else  if(!data.get(i).defaultvalue.equalsIgnoreCase("null")){
               sql += setValue(data.get(i).defaultvalue, data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }
       } 
       log("-----5");
       tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+");";
       log("-----6");
       return sql;
    }
    public String createInsert(String val1[],String val2[],String val3[],String val4[],String s1,String s2,String s3,String s4){
        log("-----1");
       String sql="insert into "+dtable+"(";
          int z = data.size();
          log("-----2");
        int col = 0;
       for(int i=0;i<z;i++){
           if(!data.get(i).destinfield.equalsIgnoreCase("null")){
               sql += data.get(i).destinfield+",";
               col++;
           }
       }
       log("-----3");
       String tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+") values(";
       int index1=0;
       int index2=0;
       int index3=0;
       int index4=0;
       int index5=0;
       log("-----4");//30
       for(int i=0;i<z;i++){
          // log("---- i : "+i);
           System.out.println("");
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
               System.out.println("source : "+data.get(i).source+" field :"+data.get(i).destinfield);
               if(data.get(i).source.equalsIgnoreCase(s1))
                 sql += setValue(val1[index1++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
               else  if(data.get(i).source.equalsIgnoreCase(s2))
                 sql += setValue(val2[index2], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
               else  if(data.get(i).source.equalsIgnoreCase(s3))
                 sql += setValue(val3[index3++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";  
               else  if(data.get(i).source.equalsIgnoreCase(s4))
                 sql += setValue(val4[index4++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }else{
               index5++;
               sql += setValue(data.get(i).defaultvalue, data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }
       } 
       System.out.println("index 1:"+index1);
       System.out.println("index 2:"+index2);
       System.out.println("index 3:"+index3);
       System.out.println("index 4:"+index4);
       System.out.println("index 5:"+index5);
       System.out.println("col :"+col+" sum index : "+(index1+index2+index3+index4+index5));
       log("-----5");
       tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+");";
       log("-----6");
       return sql;
    }  
      public String createInsert(String val[],String val2[]){
        log("-----1");
       String sql="insert into "+dtable+"(";
          int z = data.size();
          log("-----2");
       for(int i=0;i<z;i++){
           if(!data.get(i).destinfield.equalsIgnoreCase("null")){
               sql += data.get(i).destinfield+",";
           }
       }
       log("-----3");
       String tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+") values(";
       int j=0;
       int y=0;
       log("-----4");//30
       for(int i=0;i<z;i++){
          // log("---- i : "+i);
           if(!data.get(i).sourcefield.equalsIgnoreCase("null")){
               if(data.get(i).source.equalsIgnoreCase("WMS"))
                 sql += setValue(val[j++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
               else  sql += setValue(val2[y++], data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }else{
               sql += setValue(data.get(i).defaultvalue, data.get(i).type, data.get(i).length,data.get(i).destinfield, data.get(i).destination,data.get(i).defaultvalue)+",";
           }
       } 
       log("-----5");
       tmp = sql;
       sql = tmp.substring(0,tmp.length()-1)+");";
       log("-----6");
       return sql;
    }  
    
   public String getQueryLasttime(){
      return "select top 1 "+field_time+" from "+dtable+" order by "+field_time+" desc";
   }
   public String getSys(){
       return sys;
   }
      
    public  String getSqlTypeName(int type) {
        switch (type) {
            case Types.BIT:
                return "BIT";
            case Types.TINYINT:
                return "TINYINT";
            case Types.SMALLINT:
                return "SMALLINT";
            case Types.INTEGER:
                return "INTEGER";
            case Types.BIGINT:
                return "BIGINT";
            case Types.FLOAT:
                return "FLOAT";
            case Types.REAL:
                return "REAL";
            case Types.DOUBLE:
                return "DOUBLE";
            case Types.NUMERIC:
                return "NUMERIC";
            case Types.DECIMAL:
                return "DECIMAL";
            case Types.CHAR:
                return "CHAR";
            case Types.VARCHAR:
                return "VARCHAR";
            case Types.LONGVARCHAR:
                return "LONGVARCHAR";
            case Types.DATE:
                return "DATE";
            case Types.TIME:
                return "TIME";
            case Types.TIMESTAMP:
                return "TIMESTAMP";
            case Types.BINARY:
                return "BINARY";
            case Types.VARBINARY:
                return "VARBINARY";
            case Types.LONGVARBINARY:
                return "LONGVARBINARY";
            case Types.NULL:
                return "NULL";
            case Types.OTHER:
                return "OTHER";
            case Types.JAVA_OBJECT:
                return "JAVA_OBJECT";
            case Types.DISTINCT:
                return "DISTINCT";
            case Types.STRUCT:
                return "STRUCT";
            case Types.ARRAY:
                return "ARRAY";
            case Types.BLOB:
                return "BLOB";
            case Types.CLOB:
                return "CLOB";
            case Types.REF:
                return "REF";
            case Types.DATALINK:
                return "DATALINK";
            case Types.BOOLEAN:
                return "BOOLEAN";
            case Types.ROWID:
                return "ROWID";
            case Types.NCHAR:
                return "NCHAR";
            case Types.NVARCHAR:
                return "NVARCHAR";
            case Types.LONGNVARCHAR:
                return "LONGNVARCHAR";
            case Types.NCLOB:
                return "NCLOB";
            case Types.SQLXML:
                return "SQLXML";
        }
        
        return "?";
    }
      public  void findcolum(String table,String select,String values[]) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA", "sa", "P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select top 1 "+select+"  from  dbo." + table);
            rs.next();
            try {
                ResultSetMetaData rsMetaData = rs.getMetaData();
                int colum = rsMetaData.getColumnCount();
                
                System.out.println("select column : "+colum+" values column:"+values.length);
                // System.out.println("colume:"+colum);
                for (int i = 0; i < colum; i++) {
                    System.out.print(" [" + rsMetaData.getColumnName(i + 1) + " : "+values[i]+" : ");
                    System.out.print(rsMetaData.getPrecision(i + 1) + " : var length "+values[i].length() +" : ");
                    System.out.print(rsMetaData.getColumnType(i + 1) + " : " + getSqlTypeName(rsMetaData.getColumnType(i + 1)));
                    if(rsMetaData.getPrecision(i + 1)<(values[i].length()-4))
                       System.out.print(values[i]+"-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-==--=-=-=-==-=-=-");
                    System.out.println("");
                    
                }
                //int column = rsMetaData.getColumn
            } catch (SQLException ex) {
                Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (InstantiationException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
