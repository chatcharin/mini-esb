/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mailverifier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;

/**
 *
 * @author Aek
 */
public class ThSendDB extends Thread {

    String name;
    public MSSQL_Connect mSSQL_Connect = null;
    public ThSendDB(String name) {
        this.name = name;
        mSSQL_Connect = new MSSQL_Connect();
    }

    @Override
    public void run() {
        try {
          mSSQL_Connect.initTime();
           while(true){
              if(mSSQL_Connect.reset){
                  mSSQL_Connect = null;
                  mSSQL_Connect = new MSSQL_Connect();
                  mSSQL_Connect.initTime();
              }
             mSSQL_Connect.checkUpdate();
             Thread.sleep(2000);
             //Thread.sleep(2000);
           }
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }
}
