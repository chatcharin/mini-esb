/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mailverifier;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author Aek
 */
public class RunningJob {

    int tableNum = 12;
    //int[] lastCount = new int[tableNum];
    Timestamp[] lastTime = new Timestamp[tableNum];

    //table array
    ArrayList<String[]>[] tables = new ArrayList[tableNum];

    //MSSQL
    Connection con;
    
    //PostgreSQL
    Connection pcon;

    Connection gcon;
    //Message
    
    public String msg = "DataSynchro started";
    public boolean error = false;
    String wms_host, wms_dbname, wms_username, wms_passwd, wms_port, fml_host, fml_dbname, fml_username, fml_passwd, fml_port = "";

    void getProperty() {
        try {
            Properties prop = new Properties();
            prop.load(new FileInputStream("setting.properties"));
            wms_host = prop.getProperty("wms_host");
            wms_dbname = prop.getProperty("wms_dbname");
            wms_username = prop.getProperty("wms_username");
            wms_passwd = prop.getProperty("wms_passwd");
            wms_port = prop.getProperty("wms_port");
            fml_host = prop.getProperty("fml_host");
            fml_dbname = prop.getProperty("fml_dbname");
            fml_username = prop.getProperty("fml_username");
            fml_passwd = prop.getProperty("fml_passwd");
            fml_port = prop.getProperty("fml_port");
        } catch (IOException ex) {
            Logger.getLogger(setting.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }

    String pathfile;

    public RunningJob(String path) {
        try {
            loadJobs(path);
            jobsloadmap();
            getProperty();
            newTables();
            getMSSQL();
            getMySQL();
        } catch (Exception e) {
            p("Contruct Create ER:" + e.getMessage());
        }
    }

    private void jobsloadmap() {
         for(int i=0;i<job.size();i++)
             job.get(i).loadMap();
    }

    class Jobs {

        String table;
        String path;
        String type;
        String data;
        DataManagement dm;
        String mode;

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }
        public Jobs(String table, String path, String type, String data) {
            this.table = table;
            this.path = path;
            this.type = type;
            this.data = data;
            
            dm = new DataManagement(path);
        }
        public void loadMap(){
             dm = new DataManagement(path);
        }
        public DataManagement getDm() {
            return dm;
        }

        public void setDm(DataManagement dm) {
            this.dm = dm;
        }

        public String getTable() {
            return table;
        }

        public void setTable(String table) {
            this.table = table;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

    }

    ArrayList<Jobs> job = new ArrayList<Jobs>();

    public void loadJobs(String path) {
        try { 
            // "C:\\report\\mailtest.csv"
            FileInputStream fstream = new FileInputStream(path);
            // Get the object of DataInputStream
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            //Read File Line By Line
            int i = 0;
            // Read name table
            br.readLine();
            while ((strLine = br.readLine()) != null) {
                String value[] = strLine.split(",");
                p("add "+value[0]+" "+value[1]+" "+value[2]+" "+value[3]);
                job.add(new Jobs(value[0], value[1], value[2], value[3]));
                strLine = "";
            }
            br.close();
            in.close();
            fstream.close();
        } catch (Exception e) {
            p(e.getMessage());
        }
    }

    String time[] = null;

    public void newTables() {
        //new tables
        p("Start Create Tmp Table ...... ");
        tables[0] = new ArrayList<String[]>();
        tables[1] = new ArrayList<String[]>();
        tables[2] = new ArrayList<String[]>();
        tables[3] = new ArrayList<String[]>();
        tables[4] = new ArrayList<String[]>();
        tables[5] = new ArrayList<String[]>();
        tables[6] = new ArrayList<String[]>();
        tables[7] = new ArrayList<String[]>();
        tables[8] = new ArrayList<String[]>();
        tables[9] = new ArrayList<String[]>();
        tables[10] = new ArrayList<String[]>(); // new id
        tables[11] = new ArrayList<String[]>(); // new id 
        time = new String[12];
        p("Finish Create Table ......");
    }

    public void p(String txt) {
       // System.out.println("["+new Date().toString()+"]" + txt);
    }

    public void getMSSQL() {
        p("Create MSSQL  Connection ... ");
        msg = "Connecting to \nMicrosoft SQL server";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
//             con = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=formula1","sa","1234");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            pcon = DriverManager.getConnection("jdbc:sqlserver://" + wms_host + ":" + wms_port + ";databaseName=" + wms_dbname + "", wms_username, wms_passwd);
            gcon = DriverManager.getConnection("jdbc:sqlserver://" + fml_host + ":" + fml_port + ";databaseName=" + fml_dbname + "", fml_username, fml_passwd);
            con  = DriverManager.getConnection("jdbc:sqlserver://" + fml_host + ":" + fml_port + ";databaseName=" + fml_dbname + "", fml_username, fml_passwd);
        } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
            //error = true;
        }
        p("Finish Connection...");
    }
    Connection mycon;

    public void getMySQL() {
        msg = "Connecting to \nMySQL SQL ";
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            mycon = DriverManager.getConnection("jdbc:mysql://localhost:3306/map_data?useUnicode=true&characterEncoding=utf-8&zeroDateTimeBehavior=convertToNull", "root", "");
        } catch (Exception e) {
            p("MySQL ER :" + e.getMessage());
        }
    }

    public void syncData() { // check Update
        try {
            for (int m = 0; m < job.size(); m++) {
                // For SI GLREF
                //  syncDataLast(job.get(m));
                // For SI REPROD
                // syncDataMultiTable(job.get(m));
                // For SO 
                if(job.get(m).getDm().stable.equalsIgnoreCase("GLREF"))
                   syncDataLast(job.get(m)); 
                else  if(job.get(m).getDm().stable.equalsIgnoreCase("REFPROD"))
                   syncDataMultiTable(job.get(m));
                else
                   syncDataLastModified(job.get(m));
//                if(job.get(m).type.equalsIgnoreCase(""))
//                     syncDataLastModified(job.get(m));
//                else syncDataLast(job.get(m));
               // syncTest(job.get(m));
            }
            newTables();
            System.gc();
            initTime();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            msg = "Fatal error !";
            //error = true;
        }
    }

    public void initTime() {
        p("get time last update....");
        msg = "Getting last updated\ntime information";
        for (int n = 0; n < tableNum; n++) {
            // lastTime[n] = getLastTimeByField(n);
            // Test
            lastTime[n] = Timestamp.valueOf("2013-06-30 16:29:40.55");
            if (n == 6) {
                lastTime[n] = Timestamp.valueOf("2020-11-01 16:29:40.55");
            }
            if (n > 9) {
              //  lastTime[n] = getLastTimeByField(n);
              // Test
               lastTime[n] = Timestamp.valueOf("2013-06-30 16:29:40.55"); 
            }
           // System.out.println(lastTime[n].toString());
        }
    }

    public void updateLast(String table, String time) {
        try {
            Statement stmt = mycon.createStatement();
            System.out.println("INSERT INTO `map_data`.`last_update`(`id`, `table_update`, `time`) VALUES (NULL,'" + table + "','" + time + "');");
            stmt.execute("INSERT INTO `map_data`.`last_update`(`id`, `table_update`, `time`) VALUES (NULL,'" + table + "','" + time + "');");
            stmt.close();
        } catch (SQLException ex) {
            p("update lastt");
        }
    }
    public Timestamp getLastTime(Jobs job_current){
       Timestamp time = null;
        try {
            Statement stmt = null;
            ResultSet rs   = null;
            // for(int i=0;i<job_current.getDm().multisource.size();i++){
            
            // int for firt datasource
            /*-----------------------------------------------------------*/
            String source = job_current.getDm().sys;
            if(source.equalsIgnoreCase("FML"))
                stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
             System.out.println("get Time "+job_current.getDm().getQueryLasttime());
             rs = stmt.executeQuery(job_current.getDm().getQueryLasttime());
             
             if (rs.next()) 
                 time = rs.getTimestamp(1);
             if (time == null) time = Timestamp.valueOf("2013-05-01 00:00:00.0");
            rs.close();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        return time;
    }
    private static final Logger LOG = Logger.getLogger(RunningJob.class.getName());

    public Timestamp getLastTimeByField(int num) {
        Timestamp time = null;
        String[] tableNames = new String[]{"approd_detail", "order_lines", "purchase_orders", "sales_details", "sales_orders", "delivery_detail", "customer_master", "suppliers", "product_master", "product_barcode", "GLREF", "REFPROD"};
        String[] field = {"", "udf5", "remarks5", "udf5", "vessel", "delivery", "seq_group", "supplier_category", "healthcare_data", "", "FMMEMDATAA", "FCMNMGL"};
        try {
            if (!field[num].equalsIgnoreCase("")) {
                p("not emty");
                if (num < 10) {                 // Statement stmt = mycon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY,java.sql.ResultSet.CONCUR_READ_ONLY);
                    Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                    if (num == 0) {
                        //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                        String sq = "SELECT \"approved_date\" FROM \"" + tableNames[num] + "\" where \"" + field[num] + "\"='sy' ORDER BY \"approved_date\" DESC ;";
                        p(sq);
                        ResultSet prs = stmt.executeQuery(sq);
                        if (prs.next()) {
                            time = prs.getTimestamp(1);
                        }
                        if (time == null) {
                            time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                        }
                        prs.close();
                        stmt.close();
                    } else {
                        //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                        String sq = "SELECT \"last_update\" FROM \"" + tableNames[num] + "\" where \"" + field[num] + "\"='sy' ORDER BY \"last_update\" DESC ;";
                        p(sq);
                        ResultSet prs = stmt.executeQuery(sq);
                        if (prs.next()) {
                            time = prs.getTimestamp(1);
                        }
                        if (time == null) {
                            time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                        }
                        //  prs.close(); stmt.close();
                    }
                } else {
                    Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                    String sq = "SELECT \"FTLASTEDIT\" FROM \"" + tableNames[num] + "\" where \"" + field[num] + "\"='sy' ORDER BY \"FTLASTEDIT\" DESC ;";
                    p(sq);
                    ResultSet prs = stmt.executeQuery(sq);
                    if (prs.next()) {
                        time = prs.getTimestamp(1);
                    }
                    if (time == null) {
                        time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                    }
                    //  prs.close();stmt.close();
                }
            } else {
                p("isemty");
                Statement stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                if (num == 0) {
                    //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                    ResultSet prs = stmt.executeQuery("SELECT \"approved_date\" FROM \"" + tableNames[num] + "\"  ORDER BY \"approved_date\" DESC ;");
                    System.out.println("SELECT \"approved_date\" FROM \"" + tableNames[num] + "\" ORDER BY \"approved_date\" DESC ;");
                    if (prs.next()) {
                        time = prs.getTimestamp(1);
                    }
                    if (time == null) {
                        time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                    }
                    //   prs.close();stmt.close();
                } else {
                    //  ResultSet prs = stmt.executeQuery("SELECT time FROM last_update where table_update='"+tableNames[num]+"' ORDER BY \"time\" DESC ;");
                    ResultSet prs = stmt.executeQuery("SELECT \"last_update\" FROM \"" + tableNames[num] + "\" ORDER BY \"last_update\" DESC ;");
                    if (prs.next()) {
                        time = prs.getTimestamp(1);
                    }
                    if (time == null) {
                        time = Timestamp.valueOf("2013-05-01 00:00:00.0");
                    }
                    //    prs.close(); stmt.close();
                }
            }

        } catch (Exception e) {
            System.out.println(e);
            msg = "Connection failed !";
            //  error = true;
        }
        return time;
    }
    public void syncTest(Jobs job_current){
         for(int i=0;i<job_current.getDm().multisource.size();i++){
             System.out.println("Source : "+ job_current.getDm().multisource.get(i));
             try{
                 
                 String source[] = job_current.getDm().multisource.get(i).split(";");
                 System.out.println("Source :"+source[0]+" : "+source[1]);
                p("Create Query Select :"+job_current.getDm().getSelect(source[0],source[1],""));
             }catch(Exception e){
               System.out.println("Error Class RunningJob : method syncTest split source "+e.getMessage());
             }
         }
          
    }
    public void syncDataLastModified(Jobs job_current) {
        msg = "Synchronizing . . .";
        System.out.println(msg);
        ArrayList<String> sql = new ArrayList<String>();
        String time = getLastTime(job_current).toString();
        try {
            Statement stmt = null;
            ResultSet rs   = null;
            //for(int i=0;i<job_current.getDm().multisource.size();i++){
             try{
                 String source[] = job_current.getDm().multisource.get(0).split(";");
                
                 String FML      = source[0].substring(0, 3);
                 if(FML.equalsIgnoreCase("FML"))
                       stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
               
                 System.out.println("Create Query Select :"+job_current.getDm().getSelect(source[0],source[1],time));
                 rs = stmt.executeQuery(job_current.getDm().getSelect(source[0],source[1],time));
                 
                 ResultSetMetaData rsMetaData = rs.getMetaData();
                 int column = rsMetaData.getColumnCount();
                 p("start ... ");
               while (rs.next()) {
                   String[] values = new String[column];
                   for (int a = 0; a < column; a++) {
                       values[a] = rs.getString(a + 1);
                   }            
                System.out.println("Create Insert :"+job_current.getDm().createInsertLast(values));
                // sql.add(job_current.getDm().createInsert(values));
                runSQL(job_current.getDm().createInsertLast(values),job_current.getDm().sys);
               }
               
             }catch(Exception e){
                 System.out.println("Error Class RunningJob : method syncDataLastModified split source "+e.getMessage());
             }
          // }
                 //send data
            if (sql.size() > 0) {
                runQuery(sql);
            }

        } catch (Exception e) {
            System.out.println("func syncDataLAstModified "+e);
            msg = "Connection failed !";
            //  error = true;
        }
    }
    public String getFieldID(String where){
        char tmp[] = where.toCharArray();
        int firt = 0;
        int end  = 0;
        for(int i=0;i<tmp.length;i++){
            if(tmp[i]=='.')
                firt = i;
            if(tmp[i]=='}')
                end  = i;
        }
        return where.substring(firt+1, end);
    }
   
    public Vector getIdIndex(Jobs job,int index){
       Statement stmt = null;
       ResultSet rs   = null;
       Vector val = new Vector();
       String time = getLastTime(job).toString();
        try{
                 // int for firt datasource
                 /*-----------------------------------------------------------*/
                 String source[] = job.getDm().multisource.get(index).split(";");  
                 String FML      = source[0].substring(0, 3);
                 if(FML.equalsIgnoreCase("FML"))
                       stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 System.out.println(source[1]);
                 String select = source[1].substring(0,6);
                 System.out.println(source[1]+" and (last_update >CONVERT(DateTime,'"+time+"')) ORDER BY last_update ASC \n");
                 if(select.equalsIgnoreCase("select"))
                   rs = stmt.executeQuery(source[1]+" and (last_update >CONVERT(DateTime,'"+time+"')) ORDER BY last_update ASC \n");
                 else rs = stmt.executeQuery(job.getDm().getSelect(source[0],source[1],time));
                 while(rs.next()){
                     if(source.length>2){
                        String field_name[] = source[2].split(":");
                        for(String field :field_name)
                           job.getDm().id_table.put(field,rs.getString(field));
                     }
                         val.add(rs.getString(1));
                 }
                 rs.close();
                 stmt.close();
                 return val;
        }catch(Exception e){
            try {
             if(stmt!=null)  stmt.close();
             if(rs!=null)      rs.close();
           } catch (SQLException ex) {
               Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
               System.out.println("Error sql getId");
           }
             System.out.println("Error sql getId");
           return val; 
        }
    }
    
    public String[] getRecord(Jobs job,int index,String id){
       Statement stmt = null;
       ResultSet rs   = null;
       String[] val = null;
        try{
                 // int for firt datasource
                 /*-----------------------------------------------------------*/
                 String source[] = job.getDm().multisource.get(index).split(";");  
                 String FML      = source[0].substring(0, 3);
                 if(FML.equalsIgnoreCase("FML"))
                       stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 String where[]  = source[1].split("=");
                 String w =  (where.length>0)? where[0]+"='"+id+"'":source[1];
                 w = w.replaceAll(":",",");
                 System.out.println("sql get string [] :"+w);
                 rs = stmt.executeQuery(w);
                 if(rs.next()){
//                     if(source.length>2){
//                        String field_name[] = source[2].split(":");
//                        for(String field :field_name)
//                           job.getDm().id_table.put(field,rs.getString(field));
//                     }
                     int column = rs.getMetaData().getColumnCount();
                     val = new String[column+1];
                     for(int c=0;c<column;c++)
                         val[c] = rs.getString(c+1);
                 }
                 rs.close();
                 stmt.close();
                 return val;
        }catch(Exception e){
            try {
             if(stmt!=null)  stmt.close();
             if(rs!=null)      rs.close();
           } catch (SQLException ex) {
               Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
           }
           return new String[1]; 
        }
    }
     public Vector getMRecord(Jobs job,int index,String id){
       Statement stmt = null;
       ResultSet rs   = null;
      //= null;
       Vector  v = new Vector();
        try{
                 // int for firt datasource
                 /*-----------------------------------------------------------*/
                 String source[] = job.getDm().multisource.get(index).split(";");  
                 String FML      = source[0].substring(0, 3);
                
                 if(FML.equalsIgnoreCase("FML"))
                       stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 String where[]  = source[1].split("=");
                 String w = "";
                 if(source[0].equalsIgnoreCase("FML3"))
                       w =  (where.length>0)? where[0]+"=0x"+id+"":source[1];
                 else  w =  (where.length>0)? where[0]+"='"+id+"'":source[1];
                 System.out.println(job.getDm().getSelect(source[0],w,"null"));
                 rs = stmt.executeQuery(job.getDm().getSelect(source[0],w,"null"));
                 while(rs.next()){
//                     if(source.length>2){
//                        String field_name[] = source[2].split(":");
//                        for(String field :field_name)
//                           job.getDm().id_table.put(field,rs.getString(field));
//                     }
                     int column = rs.getMetaData().getColumnCount();
                     String[]  val = new String[column+1];
                     for(int c=0;c<column;c++)
                         val[c] = rs.getString(c+1);
                     v.add(val);
                 }
                 rs.close();
                 stmt.close();
                 return v;
        }catch(Exception e){
            try {
             if(stmt!=null)  stmt.close();
             if(rs!=null)      rs.close();
           } catch (SQLException ex) {
               Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
           }
           return v; 
        }
    }
    public void syncDataMultiTable(Jobs job_current){
        try{
           Vector v        = getIdIndex(job_current,0);
           String source[] = new String[job_current.getDm().multisource.size()];
           for(int s=0;s<job_current.getDm().multisource.size();s++)
               source[s] = job_current.getDm().multisource.get(s).split(";")[0];
           for(int index=0;index<v.size();index++)
           {
              try{ 
               String val1[]= getRecord(job_current, 1,(String)v.get(index));
               String val2[]= getRecord(job_current, 2,(String)v.get(index));
               Vector a = getMRecord(job_current, 3,(String)v.get(index));
               Vector b = getMRecord(job_current, 4,val1[1]);
                  System.out.println("start 1 .... ");
               for(int n=0;n<a.size();n++){
                try{   
                 System.out.println("start 2.... ");
                 String val3[] = (String[])a.get(n);
                  System.out.println("start 3.... ");
                 String val4[] = (String[])b.get(n);
                  System.out.println("start 4.... "+job_current.getDm().dtable);
                   String id = new_id(job_current.getDm().dtable);
                    System.out.println("start 5.... ");
                   String sql_insert = job_current.getDm().createInsert(val1, val2, val3, val4, source[1],source[2] ,source[3], source[4]).replaceAll("'genid'",id);
                 System.out.println(sql_insert);
                 System.out.println("end ......");
                 runSQL(sql_insert,job_current.getDm().sys);
                }catch(Exception e){
                    System.out.println("ER insert multi"+e.getMessage());}
                }
                }catch(Exception e){System.out.println("ER insert multi get Vector val1 val2 a b : "+e.getMessage());}
               //create SQL insert  val1 , val2 , val3 ,val4
           }
        }catch(Exception e){
            System.out.println("Error multitalbe "+e.getMessage());
        }
    }
     public String getNewRunnign(String bookid){
         String newruning = null;
        try {
            Statement stmt = null;
            ResultSet rs   = null;
            stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
          
           
             rs = stmt.executeQuery("SELECT TOP 1 SUBSTRING( cast((SUBSTRING([FCREFNO],9,15)+1) as varchar (8)),5,8)\n" +
                 ",[FCREFNO] " +
                 " FROM [formula].[dbo].[GLREF] where FCREFNO like 'SI-%' and FCREFNO like '%"+bookid+"%'  order by FCSKID desc ");
             
             if (rs.next()) 
                 newruning = rs.getString(1);
             if (newruning == null) newruning = "0001";
            rs.close();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(" New Runing number :"+newruning);
        return newruning;
    }
      public String getBookID(String bookid){
         String newruning = null;
        try {
            Statement stmt = null;
            ResultSet rs   = null;
            stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
          
           
             rs = stmt.executeQuery("SELECT cast(FCSKID as binary(8)) as 'FCSKID' from BOOK where FCCODE = '"+bookid.trim()+"%' and FCREFTYPE = 'SI'");
             
             if (rs.next()) 
                 newruning = rs.getString(1);
             if (newruning == null) newruning = "0001";
            rs.close();
            stmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(" New Runing number :"+newruning);
        return newruning;
    }
    SimpleDateFormat fmd = new SimpleDateFormat("yyMM");
    public  String getyyMM() {
        return fmd.format(new java.util.Date());
    }
    public void syncDataLast(Jobs job_current) {
        msg = "Synchronizing . . .";
        System.out.println(msg);
        ArrayList<String> sql = new ArrayList<String>();
        ArrayList<String[]> tmp = new ArrayList<String[]>();
        String time = getLastTime(job_current).toString();
        try {
            Statement stmt = null;
            ResultSet rs   = null;
           // for(int i=0;i<job_current.getDm().multisource.size();i++){
             try{
                 // int for firt datasource
                 /*-----------------------------------------------------------*/
                 String source[] = job_current.getDm().multisource.get(0).split(";");
                
                 String FML      = source[0].substring(0, 3);
                 if(FML.equalsIgnoreCase("FML"))
                       stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                 else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
               
                 /*------------------------------------------------------------*/
                 
                 // get Index Id for Second Datasource
                 // Find Data From scecod Datasource 
                   String source2[] = job_current.getDm().multisource.get(1).split(";");
                   String    WMS    = source2[0].substring(0, 3);
                   Statement stmt2  = null;
                   ResultSet rs2    = null;
                   // Create statement 
                  if(WMS.equalsIgnoreCase("WMS"))
                        stmt2 = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                  else  stmt2 = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                  
                  String field_id =  getFieldID(source2[1]);
                  String where[]  = source2[1].split("=");
                  //job_current.getDm().setFindIndex(field_id);
                  /*------------------------------------------------------------*/
                 
                 
                 System.out.println("Create Query Select :"+job_current.getDm().getSelect(source[0],source[1],time));
                 rs = stmt.executeQuery(job_current.getDm().getSelect(source[0],source[1],time));
                // int index_id = job_current.getDm().getIndexId();
                 ResultSetMetaData rsMetaData = rs.getMetaData();
                 int column = rsMetaData.getColumnCount();
                 p("start ... ");
               int row = 0;
               while (rs.next()) {
                   p("job size "+job_current.getDm().size);
                   // Create Array for data row
                   String[] values = new String[job_current.getDm().size+10];
                   String val1[] = new String[column+1];
                  String w_j = where[0]+"='"+rs.getString(field_id)+"'";
                  System.out.println("Debug Second Data SQL :"+job_current.getDm().getSelect(source2[0],w_j,"null",",(select BOOK.FCCODE from BOOK where BOOK.FCSKID=ORDERH.FCBOOK) as BOOKCODE "));
                   rs2 = stmt2.executeQuery(job_current.getDm().getSelect(source2[0],w_j,"null",",(select BOOK.FCCODE from BOOK where BOOK.FCSKID=ORDERH.FCBOOK) as BOOKCODE "));
                   int column2 = rs2.getMetaData().getColumnCount();
                   String val2[] = new String[column2+1];
                   
                   for(int a=0;a<column;a++)
                       val1[a] = rs.getString(a+1);
                   rs2.next();
                   for(int b=0;b<column2;b++)
                       val2[b] = rs2.getString(b+1);
                   
                   String id = new_id(job_current.getDm().dtable);
                   String runing = getNewRunnign(rs2.getString("BOOKCODE"));
                   String sql_insert = job_current.getDm().createInsert(val1,val2).replaceAll("'genid'",id);
                   sql_insert  = sql_insert.replaceAll("fccode",getyyMM()+runing);
                   sql_insert  = sql_insert.replaceAll("fcrefno","SI-"+rs2.getString("BOOKCODE").trim()+"/"+getyyMM()+runing);
                   sql_insert  = sql_insert.replaceAll("'fcbook'","0x"+getBookID(rs2.getString("BOOKCODE").trim()));
                   
                   System.out.println("Insert SQL : " +sql_insert);
                   runSQL(sql_insert,job_current.getDm().sys);
                   row++;
                
               // sql.add(job_current.getDm().createInsert(values));
               }
               /*
               for(int e=0;e<tmp.size();e++){
                   p("Create Insert :"+job_current.getDm().createInsert(tmp.get(e)));
               }*/
             }catch(Exception e){
                 System.out.println("Error Class RunningJob : method syncDataLastModified split source "+e.getMessage());
             }
           
 
            
           
            //send data
            if (sql.size() > 0) {
                runQuery(sql);
            }

        } catch (Exception e) {
            System.out.println("func syncDataLAstModified"+e);
            msg = "Connection failed !";
            //  error = true;
        }
    }
    


    String system[] = {"WMS", "WMS", "WMS", "WMS", "WMS", "WMS", "WMS", "WMS", "WMS", "WMS", "Formula", "Formula"};
    String table[] = {"approd_detail", "order_lines", "purchase_orders", "sales_details", "sales_orders", "delivery_detail", "customer_master", "suppliers", "product_master", "product_barcode", "GLREF", "REFPROD"};

    public String ReSymbol(String txt) {
        String tmp_sql = txt.replaceAll("\"", "\\\\\"");
        String sql_er = tmp_sql.replaceAll("'", "\\\\'");
        return sql_er;
    }

    public String getSQL(String sys, String tbl, String sql, String er) {
        //WHERE NOT EXISTS ( SELECT * FROM users nx WHERE nx.ip = xx -- KEYFIELDS);
        return "INSERT INTO `map_data`.`data_log` (`id`,`system`, `table`, `sql_txt`, `error`, `time`) VALUES (NULL, '" + sys + "', '" + tbl + "', '" + ReSymbol(sql) + "', '" + ReSymbol(er) + "', CURRENT_TIMESTAMP) ;";
    }

    public void runQuery(ArrayList<String> sql) {
        msg = "Updating . . .";
        for (int i = 0; i < sql.size(); i++) {
            try {
                Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                p("Query : "+sql.get(i));
                stmt.execute(sql.get(i));
                stmt.close();
            } catch (Exception e) {
                p("error get field");
            }
        }
    }
   public void runSQL(String sql,String sys){
       Statement stmt = null ;
       try {
           
            if(sys.equalsIgnoreCase("FML"))
                  stmt = gcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            else  stmt = pcon.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            stmt.execute(sql);
            stmt.close();
        } catch (SQLException ex) {
            
            Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex);
            try {
                stmt.close();
            } catch (SQLException ex1) {
                Logger.getLogger(RunningJob.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
   }
    public String getField(String sql) {
        String field = "";
        try {
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                field = rs.getString(1);
            }
            rs.close();
            stmt.close();

        } catch (Exception e) {
            p("error get field");
        }
        return field;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyMMHHmmssSS");

    public String getGLfccode(String table) {
        return sdf.format(new java.util.Date());
    }

    //create startup registry
    void createRegistry() {
        try {
            msg = "Creating starts up point";

            //find current location
            URL location = getClass().getProtectionDomain().getCodeSource().getLocation();
            String path = (location.getPath().replace("%20", " ")).replace("/", "\\");
            path = path.substring(1, path.length());

            Process cmd = Runtime.getRuntime().exec("reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Run /v DataSynchro /t REG_SZ /d \"" + path + "\"");
            OutputStreamWriter osw = new OutputStreamWriter(cmd.getOutputStream());
            osw.write("y");
            osw.flush();
            osw.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(cmd.getInputStream()));

            String line = "";
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String new_id(String table) {
        int i = 0;
        int j = 0;
        String new_id = "0x";
        String code = "";
        Connection cons;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            // cons = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA","sa","P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select  CAST(MAX(dbo." + table + ".FCSKID) AS binary(8)) from  dbo." + table + " ");
            p("select CAST(MAX(dbo." + table + ".FCSKID) AS binary(8))  from  dbo." + table + " ");
            // ResultSetMetaData rsMetaData = rs.getMetaData();
            //int column = rsMetaData.getColumnCount();
            if (rs.next()) {
                code = rs.getString(1);
                System.out.println("First :" + code);
            }
            // code ="FFFFFFFFFFFFFFFF";
            System.out.println("Test :" + code);
            String tmp = "";
            String work = "";
            for (int a = 0; a < 8; a++) {
                work = code.substring(16 - ((a + 1) * 2), 16 - (a * 2));
                tmp = code;
                int hex = Integer.decode("#" + work);
                int next = 0;
                if (getValue(hex) != 207) {
                    next = (getValue(hex) + 1);
                }

                System.out.println(" new id : " + getHEX(next) + " : " + Integer.toHexString(getHEX(next)).toUpperCase());
                if (a == 0) {
                    code = tmp.substring(0, 16 - ((a + 1) * 2)) + Integer.toHexString(getHEX(next)).toUpperCase();
                } else {
                    code = tmp.substring(0, 16 - ((a + 1) * 2)) + Integer.toHexString(getHEX(next)).toUpperCase() + tmp.substring(16 - ((a) * 2), 16);
                    System.out.println("1 :" + tmp.substring(0, 16 - ((a + 1) * 2)));
                    System.out.println("2 :" + Integer.toHexString(getHEX(next)).toUpperCase());
                    System.out.println("3 :" + tmp.substring(16 - ((a) * 2), 16));
                }
                System.out.println("HEX Ox" + code);
                if (next != 0) {
                    break;
                }
            }

            return "0x" + code;

        } catch (Exception e) {
            p("create id error :" + e);
            return "0x" + code;
        }
    }

    String pk[] = {"(", ")", "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", "4", "5", ")", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "[", "\\", "]", "^", "_", "`", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "{", "|", "}", "~", "", "�", "�", "�", "�", "…", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "‘", "’", "“", "”", "•", "–", "—", "�", "�", "�", "�", "�", "�", "�", "�", " ", "ก", "ข", "ฃ", "ค", "ฅ", "ฆ", "ง", "จ", "ฉ", "ช", "ซ", "ฌ", "ญ", "ฎ", "ฏ", "ฐ", "ฑ", "ฒ", "ณ", "ด", "ต", "ถ", "ท", "ธ", "น", "บ", "ป", "ผ", "ฝ", "พ", "ฟ", "ภ", "ม", "ย", "ร", "ฤ", "ล", "ฦ", "ว", "ศ", "ษ", "ส", "ห", "ฬ", "อ", "ฮ", "ะ", "ั", "า", "ำ", "ิ", "ี", "ึ", "ื", "ุ", "ู", "ฺ", "�", "�", "�", "�", "฿", "เ", "แ", "โ", "ใ", "ไ", "ๅ", "ๆ", "ํ", "๎", "๏", "๐", "๑", "๒", "๓", "๔", "๕", "๖", "๗", "๘", "๙", "๚", "๛", "�", "�", "�", "�"};

    public int getHEX(int c) {
        switch (c) {
            case 0:
                return 0x28;
            case 1:
                return 0x29;
            case 2:
                return 0x2A;
            case 3:
                return 0x2B;
            case 4:
                return 0x2C;
            case 5:
                return 0x2D;
            case 6:
                return 0x2E;
            case 7:
                return 0x2F;
            case 8:
                return 0x30;
            case 9:
                return 0x31;
            case 10:
                return 0x32;
            case 11:
                return 0x33;
            case 12:
                return 0x34;
            case 13:
                return 0x35;
            case 14:
                return 0x36;
            case 15:
                return 0x37;
            case 16:
                return 0x38;
            case 17:
                return 0x39;
            case 18:
                return 0x3A;
            case 19:
                return 0x3B;
            case 20:
                return 0x3C;
            case 21:
                return 0x3D;
            case 22:
                return 0x3E;
            case 23:
                return 0x3F;
            case 24:
                return 0x40;
            case 25:
                return 0x41;
            case 26:
                return 0x42;
            case 27:
                return 0x43;
            case 28:
                return 0x44;
            case 29:
                return 0x45;
            case 30:
                return 0x46;
            case 31:
                return 0x47;
            case 32:
                return 0x48;
            case 33:
                return 0x49;
            case 34:
                return 0x4A;
            case 35:
                return 0x4B;
            case 36:
                return 0x4C;
            case 37:
                return 0x4D;
            case 38:
                return 0x4E;
            case 39:
                return 0x4F;
            case 40:
                return 0x50;
            case 41:
                return 0x51;
            case 42:
                return 0x52;
            case 43:
                return 0x53;
            case 44:
                return 0x54;
            case 45:
                return 0x55;
            case 46:
                return 0x56;
            case 47:
                return 0x57;
            case 48:
                return 0x58;
            case 49:
                return 0x59;
            case 50:
                return 0x5A;
            case 51:
                return 0x5B;
            case 52:
                return 0x5C;
            case 53:
                return 0x5D;
            case 54:
                return 0x5E;
            case 55:
                return 0x5F;
            case 56:
                return 0x60;
            case 57:
                return 0x61;
            case 58:
                return 0x62;
            case 59:
                return 0x63;
            case 60:
                return 0x64;
            case 61:
                return 0x65;
            case 62:
                return 0x66;
            case 63:
                return 0x67;
            case 64:
                return 0x68;
            case 65:
                return 0x69;
            case 66:
                return 0x6A;
            case 67:
                return 0x6B;
            case 68:
                return 0x6C;
            case 69:
                return 0x6D;
            case 70:
                return 0x6E;
            case 71:
                return 0x6F;
            case 72:
                return 0x70;
            case 73:
                return 0x71;
            case 74:
                return 0x72;
            case 75:
                return 0x73;
            case 76:
                return 0x74;
            case 77:
                return 0x75;
            case 78:
                return 0x76;
            case 79:
                return 0x77;
            case 80:
                return 0x78;
            case 81:
                return 0x79;
            case 82:
                return 0x7A;
            case 83:
                return 0x7B;
            case 84:
                return 0x7C;
            case 85:
                return 0x7D;
            case 86:
                return 0x7E;
            case 87:
                return 0x7F;
            case 88:
                return 0x81;
            case 89:
                return 0x82;
            case 90:
                return 0x83;
            case 91:
                return 0x84;
            case 92:
                return 0x85;
            case 93:
                return 0x86;
            case 94:
                return 0x87;
            case 95:
                return 0x88;
            case 96:
                return 0x89;
            case 97:
                return 0x8A;
            case 98:
                return 0x8B;
            case 99:
                return 0x8C;
            case 100:
                return 0x8D;
            case 101:
                return 0x8E;
            case 102:
                return 0x8F;
            case 103:
                return 0x90;
            case 104:
                return 0x91;
            case 105:
                return 0x92;
            case 106:
                return 0x93;
            case 107:
                return 0x94;
            case 108:
                return 0x95;
            case 109:
                return 0x96;
            case 110:
                return 0x97;
            case 111:
                return 0x98;
            case 112:
                return 0x99;
            case 113:
                return 0x9A;
            case 114:
                return 0x9B;
            case 115:
                return 0x9C;
            case 116:
                return 0x9D;
            case 117:
                return 0x9E;
            case 118:
                return 0x9F;
            case 119:
                return 0xA0;
            case 120:
                return 0xA1;
            case 121:
                return 0xA2;
            case 122:
                return 0xA3;
            case 123:
                return 0xA4;
            case 124:
                return 0xA5;
            case 125:
                return 0xA6;
            case 126:
                return 0xA7;
            case 127:
                return 0xA8;
            case 128:
                return 0xA9;
            case 129:
                return 0xAA;
            case 130:
                return 0xAB;
            case 131:
                return 0xAC;
            case 132:
                return 0xAD;
            case 133:
                return 0xAE;
            case 134:
                return 0xAF;
            case 135:
                return 0xB0;
            case 136:
                return 0xB1;
            case 137:
                return 0xB2;
            case 138:
                return 0xB3;
            case 139:
                return 0xB4;
            case 140:
                return 0xB5;
            case 141:
                return 0xB6;
            case 142:
                return 0xB7;
            case 143:
                return 0xB8;
            case 144:
                return 0xB9;
            case 145:
                return 0xBA;
            case 146:
                return 0xBB;
            case 147:
                return 0xBC;
            case 148:
                return 0xBD;
            case 149:
                return 0xBE;
            case 150:
                return 0xBF;
            case 151:
                return 0xC0;
            case 152:
                return 0xC1;
            case 153:
                return 0xC2;
            case 154:
                return 0xC3;
            case 155:
                return 0xC4;
            case 156:
                return 0xC5;
            case 157:
                return 0xC6;
            case 158:
                return 0xC7;
            case 159:
                return 0xC8;
            case 160:
                return 0xC9;
            case 161:
                return 0xCA;
            case 162:
                return 0xCB;
            case 163:
                return 0xCC;
            case 164:
                return 0xCD;
            case 165:
                return 0xCE;
            case 166:
                return 0xD0;
            case 167:
                return 0xD1;
            case 168:
                return 0xD2;
            case 169:
                return 0xD3;
            case 170:
                return 0xD4;
            case 171:
                return 0xD5;
            case 172:
                return 0xD6;
            case 173:
                return 0xD7;
            case 174:
                return 0xD8;
            case 175:
                return 0xD9;
            case 176:
                return 0xDA;
            case 177:
                return 0xDB;
            case 178:
                return 0xDC;
            case 179:
                return 0xDD;
            case 180:
                return 0xDE;
            case 181:
                return 0xDF;
            case 182:
                return 0xE0;
            case 183:
                return 0xE1;
            case 184:
                return 0xE2;
            case 185:
                return 0xE3;
            case 186:
                return 0xE4;
            case 187:
                return 0xE5;
            case 188:
                return 0xE6;
            case 189:
                return 0xED;
            case 190:
                return 0xEE;
            case 191:
                return 0xEF;
            case 192:
                return 0xF0;
            case 193:
                return 0xF1;
            case 194:
                return 0xF2;
            case 195:
                return 0xF3;
            case 196:
                return 0xF4;
            case 197:
                return 0xF5;
            case 198:
                return 0xF6;
            case 199:
                return 0xF7;
            case 200:
                return 0xF8;
            case 201:
                return 0xF9;
            case 202:
                return 0xFA;
            case 203:
                return 0xFB;
            case 204:
                return 0xFC;
            case 205:
                return 0xFD;
            case 206:
                return 0xFE;
            case 207:
                return 0xFF;
        }
        return -1;
    }

    public int getValue(int x) {
        switch (x) {
            case 40:
                return 0;
            case 41:
                return 1;
            case 42:
                return 2;
            case 43:
                return 3;
            case 44:
                return 4;
            case 45:
                return 5;
            case 46:
                return 6;
            case 47:
                return 7;
            case 48:
                return 8;
            case 49:
                return 9;
            case 50:
                return 10;
            case 51:
                return 11;
            case 52:
                return 12;
            case 53:
                return 13;
            case 54:
                return 14;
            case 55:
                return 15;
            case 56:
                return 16;
            case 57:
                return 17;
            case 58:
                return 18;
            case 59:
                return 19;
            case 60:
                return 20;
            case 61:
                return 21;
            case 62:
                return 22;
            case 63:
                return 23;
            case 64:
                return 24;
            case 65:
                return 25;
            case 66:
                return 26;
            case 67:
                return 27;
            case 68:
                return 28;
            case 69:
                return 29;
            case 70:
                return 30;
            case 71:
                return 31;
            case 72:
                return 32;
            case 73:
                return 33;
            case 74:
                return 34;
            case 75:
                return 35;
            case 76:
                return 36;
            case 77:
                return 37;
            case 78:
                return 38;
            case 79:
                return 39;
            case 80:
                return 40;
            case 81:
                return 41;
            case 82:
                return 42;
            case 83:
                return 43;
            case 84:
                return 44;
            case 85:
                return 45;
            case 86:
                return 46;
            case 87:
                return 47;
            case 88:
                return 48;
            case 89:
                return 49;
            case 90:
                return 50;
            case 91:
                return 51;
            case 92:
                return 52;
            case 93:
                return 53;
            case 94:
                return 54;
            case 95:
                return 55;
            case 96:
                return 56;
            case 97:
                return 57;
            case 98:
                return 58;
            case 99:
                return 59;
            case 100:
                return 60;
            case 101:
                return 61;
            case 102:
                return 62;
            case 103:
                return 63;
            case 104:
                return 64;
            case 105:
                return 65;
            case 106:
                return 66;
            case 107:
                return 67;
            case 108:
                return 68;
            case 109:
                return 69;
            case 110:
                return 70;
            case 111:
                return 71;
            case 112:
                return 72;
            case 113:
                return 73;
            case 114:
                return 74;
            case 115:
                return 75;
            case 116:
                return 76;
            case 117:
                return 77;
            case 118:
                return 78;
            case 119:
                return 79;
            case 120:
                return 80;
            case 121:
                return 81;
            case 122:
                return 82;
            case 123:
                return 83;
            case 124:
                return 84;
            case 125:
                return 85;
            case 126:
                return 86;
            case 127:
                return 87;
            case 129:
                return 88;
            case 130:
                return 89;
            case 131:
                return 90;
            case 132:
                return 91;
            case 133:
                return 92;
            case 134:
                return 93;
            case 135:
                return 94;
            case 136:
                return 95;
            case 137:
                return 96;
            case 138:
                return 97;
            case 139:
                return 98;
            case 140:
                return 99;
            case 141:
                return 100;
            case 142:
                return 101;
            case 143:
                return 102;
            case 144:
                return 103;
            case 145:
                return 104;
            case 146:
                return 105;
            case 147:
                return 106;
            case 148:
                return 107;
            case 149:
                return 108;
            case 150:
                return 109;
            case 151:
                return 110;
            case 152:
                return 111;
            case 153:
                return 112;
            case 154:
                return 113;
            case 155:
                return 114;
            case 156:
                return 115;
            case 157:
                return 116;
            case 158:
                return 117;
            case 159:
                return 118;
            case 160:
                return 119;
            case 161:
                return 120;
            case 162:
                return 121;
            case 163:
                return 122;
            case 164:
                return 123;
            case 165:
                return 124;
            case 166:
                return 125;
            case 167:
                return 126;
            case 168:
                return 127;
            case 169:
                return 128;
            case 170:
                return 129;
            case 171:
                return 130;
            case 172:
                return 131;
            case 173:
                return 132;
            case 174:
                return 133;
            case 175:
                return 134;
            case 176:
                return 135;
            case 177:
                return 136;
            case 178:
                return 137;
            case 179:
                return 138;
            case 180:
                return 139;
            case 181:
                return 140;
            case 182:
                return 141;
            case 183:
                return 142;
            case 184:
                return 143;
            case 185:
                return 144;
            case 186:
                return 145;
            case 187:
                return 146;
            case 188:
                return 147;
            case 189:
                return 148;
            case 190:
                return 149;
            case 191:
                return 150;
            case 192:
                return 151;
            case 193:
                return 152;
            case 194:
                return 153;
            case 195:
                return 154;
            case 196:
                return 155;
            case 197:
                return 156;
            case 198:
                return 157;
            case 199:
                return 158;
            case 200:
                return 159;
            case 201:
                return 160;
            case 202:
                return 161;
            case 203:
                return 162;
            case 204:
                return 163;
            case 205:
                return 164;
            case 206:
                return 165;
            case 208:
                return 166;
            case 209:
                return 167;
            case 210:
                return 168;
            case 211:
                return 169;
            case 212:
                return 170;
            case 213:
                return 171;
            case 214:
                return 172;
            case 215:
                return 173;
            case 216:
                return 174;
            case 217:
                return 175;
            case 218:
                return 176;
            case 219:
                return 177;
            case 220:
                return 178;
            case 221:
                return 179;
            case 222:
                return 180;
            case 223:
                return 181;
            case 224:
                return 182;
            case 225:
                return 183;
            case 226:
                return 184;
            case 227:
                return 185;
            case 228:
                return 186;
            case 229:
                return 187;
            case 230:
                return 188;
            case 237:
                return 189;
            case 238:
                return 190;
            case 239:
                return 191;
            case 240:
                return 192;
            case 241:
                return 193;
            case 242:
                return 194;
            case 243:
                return 195;
            case 244:
                return 196;
            case 245:
                return 197;
            case 246:
                return 198;
            case 247:
                return 199;
            case 248:
                return 200;
            case 249:
                return 201;
            case 250:
                return 202;
            case 251:
                return 203;
            case 252:
                return 204;
            case 253:
                return 205;
            case 254:
                return 206;
            case 255:
                return 207;
        }
        return 0;
    }
}
