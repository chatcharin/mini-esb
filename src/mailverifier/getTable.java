/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mailverifier;

import static ccsthread.getUnicode.getHEX;
import static ccsthread.getUnicode.getValue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Types;

/**
 *
 * @author Aek
 */
public class getTable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //SELECT * FROM sys.tables
        ArrayList<String> a = new ArrayList<String>();
        try {
            // insert into Files (FileId, FileData) values (1, 0x010203040506)
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA", "sa", "P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("SELECT  * FROM sys.tables");
            // ResultSetMetaData rsMetaData = rs.getMetaData();
            //int column = rsMetaData.getColumnCount();
            int i = 0;
            int j = 0;
            
            while (rs.next()) {
                String s = new String(rs.getString(1).getBytes(), "UTF-8");
                String code = rs.getString(1);
                
                a.add(code);
                //System.out.println(" "+(j++)+":"+s.codePointAt(7)+" HEX :"+code+" DEC :"+Long.decode("#"+code)+" : "+rs.getString(2));
                // System.out.println("Table :"+code+";");  
                try {
                    // findcolum(code);
                } catch (Exception e) {
                }
                i++;
            }
            for (int x = 0; x < i; x++) {
                
                try {
                    
                    System.out.println("");
                    System.out.print("[" + a.get(x) + " ]:");
                    findcolum(a.get(x));
                } catch (Exception e) {
                    System.out.println("" + e.getMessage());
                }
            }
            // long a = Long.decode("0x75B0753A2828282A00000000000000000000000000000000000000000000");
            // System.out.println(""+a);
            rs.close();
            stmt.close();
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }    

    public static String getSqlTypeName(int type) {
        switch (type) {
            case Types.BIT:
                return "BIT";
            case Types.TINYINT:
                return "TINYINT";
            case Types.SMALLINT:
                return "SMALLINT";
            case Types.INTEGER:
                return "INTEGER";
            case Types.BIGINT:
                return "BIGINT";
            case Types.FLOAT:
                return "FLOAT";
            case Types.REAL:
                return "REAL";
            case Types.DOUBLE:
                return "DOUBLE";
            case Types.NUMERIC:
                return "NUMERIC";
            case Types.DECIMAL:
                return "DECIMAL";
            case Types.CHAR:
                return "CHAR";
            case Types.VARCHAR:
                return "VARCHAR";
            case Types.LONGVARCHAR:
                return "LONGVARCHAR";
            case Types.DATE:
                return "DATE";
            case Types.TIME:
                return "TIME";
            case Types.TIMESTAMP:
                return "TIMESTAMP";
            case Types.BINARY:
                return "BINARY";
            case Types.VARBINARY:
                return "VARBINARY";
            case Types.LONGVARBINARY:
                return "LONGVARBINARY";
            case Types.NULL:
                return "NULL";
            case Types.OTHER:
                return "OTHER";
            case Types.JAVA_OBJECT:
                return "JAVA_OBJECT";
            case Types.DISTINCT:
                return "DISTINCT";
            case Types.STRUCT:
                return "STRUCT";
            case Types.ARRAY:
                return "ARRAY";
            case Types.BLOB:
                return "BLOB";
            case Types.CLOB:
                return "CLOB";
            case Types.REF:
                return "REF";
            case Types.DATALINK:
                return "DATALINK";
            case Types.BOOLEAN:
                return "BOOLEAN";
            case Types.ROWID:
                return "ROWID";
            case Types.NCHAR:
                return "NCHAR";
            case Types.NVARCHAR:
                return "NVARCHAR";
            case Types.LONGNVARCHAR:
                return "LONGNVARCHAR";
            case Types.NCLOB:
                return "NCLOB";
            case Types.SQLXML:
                return "SQLXML";
        }
        
        return "?";
    }

    public static void findcolum(String table) {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA", "sa", "P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select top 1 *  from  dbo." + table);
            rs.next();
            try {
                ResultSetMetaData rsMetaData = rs.getMetaData();
                int colum = rsMetaData.getColumnCount();
                // System.out.println("colume:"+colum);
                for (int i = 0; i < colum; i++) {
                    System.out.print(" [" + rsMetaData.getColumnName(i + 1) + " : ");
                    System.out.print(rsMetaData.getPrecision(i + 1) + " : ");
                    System.out.print(rsMetaData.getColumnType(i + 1) + " : " + getSqlTypeName(rsMetaData.getColumnType(i + 1)));
                }
                //int column = rsMetaData.getColumn
            } catch (SQLException ex) {
                Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
            }
            rs.close();
            stmt.close();
            con.close();
        } catch (InstantiationException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(getTable.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void new_id() {
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            Connection con = DriverManager.getConnection("jdbc:sqlserver://110.170.169.205:1433;databaseName=FORMULA", "sa", "P@ssword");
//             pcon = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1:1433;databaseName=similan","sa","1234");
            Statement stmt = con.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            // ResultSet rs = stmt.executeQuery("select top 1000 dbo.GLREF.FCSKID,CAST(dbo.GLREF.FCSKID AS binary(8)) AS 'Copies'  from  dbo.GLREF order by  dbo.GLREF.FCSKID ");
            ResultSet rs = stmt.executeQuery("select top 1 dbo.PROD.FCSKID,CAST(dbo.PROD.FCSKID AS binary(8)) AS 'Copies'  from  dbo.PROD order by  dbo.PROD.FCSKID ");
            // ResultSetMetaData rsMetaData = rs.getMetaData();
            //int column = rsMetaData.getColumnCount();
            int i = 0;
            int j = 0;
            if (rs.next()) {
                String code = rs.getString(2).substring(14, 16);
                int hex = Integer.decode("#" + code);
                int next = 0;
                if (getValue(hex) != 207) {
                    next = getValue(hex);
                }
                System.out.println(" new id : " + getHEX(next));
            }
            while (rs.next()) {
                String s = new String(rs.getString(1).getBytes(), "UTF-8");
                String code = rs.getString(2).substring(14, 16);
                
                if (i > 621 && i < 830) {
                    //System.out.println(" "+(j++)+":"+s.codePointAt(7)+" HEX :"+code+" DEC :"+Long.decode("#"+code)+" : "+rs.getString(2));
                    System.out.println("case " + Integer.decode("#" + code) + ":return " + (j++) + ";");                    
                }
                i++;
            }

            // long a = Long.decode("0x75B0753A2828282A00000000000000000000000000000000000000000000");
            // System.out.println(""+a);
            rs.close();
            stmt.close();
            
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
}
