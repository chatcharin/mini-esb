@ECHO OFF
set DIRNAME=%~dp0
set CP="%DIRNAME%CCSThread.jar"
set CP=%CP%;"%DIRNAME%lib/mysql-connector-java-5.1.24-bin.jar"
set CP=%CP%;"%DIRNAME%lib/postgresql-9.2-1002.jdbc4.jar"
set CP=%CP%;"%DIRNAME%lib/sqljdbc4.jar"
start /B javaw -cp %CP% -Xms64m -Xmx256m -Xnoclassgc ccsthread.CCSThread